VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} addItemForm 
   Caption         =   "���������� ��������"
   ClientHeight    =   5460
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   15375
   OleObjectBlob   =   "addItemForm.frx":0000
   ShowModal       =   0   'False
   StartUpPosition =   2  'CenterScreen
End
Attribute VB_Name = "addItemForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub btn_AddItem_Click()         '���������� ��������� ������ � ��������������� ����
    
    On Error GoTo instr1
    
    Dim sheet_name As String
    Dim num As Long
    
    If ob_RootAssembly.value = True Then
    
        sheet_name = "arr_RootAssemblies"
    
        If if_RootAssembly_Filled(textBox_Code.value, textBox_Name, textBox_Amount) = True And _
           if_doubled(textBox_Code.value, textBox_In.value, textBox_GOST.value, ActiveWorkbook.Worksheets("arr_RootAssemblies")) = False Then
                write_element ActiveWorkbook.Worksheets("arr_RootAssemblies"), num_RootAssemblies
                listbox_update_added sheet_name, textBox_Code, textBox_Name
                clean_the_form
        Else
           If if_RootAssembly_Filled(textBox_Code.value, textBox_Name, textBox_Amount) = False Then
                MsgBox "��������� ��� ����������� ����"
           ElseIf if_doubled(textBox_Code.value, textBox_In.value, textBox_GOST.value, ActiveWorkbook.Worksheets("arr_RootAssemblies")) = True Then
                MsgBox "������� � ����� ������������ � ���������� ��� ����������!"
           End If
        End If
    End If
    
    If ob_SubAssembly.value = True Or ob_Part.value = True Then
    
        If ob_SubAssembly.value = True Then sheet_name = "arr_SubAssemblies"    '����� � ����� ���� ��������� ��� �������� ��� ����� ��� �� ��� � ��� ������ ��� ���������� ����� ��������������,
                                                                '������ �������� ��������� � ����������� ������������ � ������������� ����� � ���� ���������
                                                                '�������� ���������� ���������, �� ��������. � ��� �� � �� ����������� :)
        If ob_Part.value = True Then sheet_name = "arr_Parts"
            
        
        If if_SubAssemblyOrPart_Filled(textBox_Code, textBox_Name, textBox_In, textBox_Amount) = True And _
           (if_In_Assembly_exists(textBox_In, ActiveWorkbook.Worksheets("arr_RootAssemblies")) = True Or if_In_Assembly_exists(textBox_In, ActiveWorkbook.Worksheets("arr_SubAssemblies")) = True) And _
           if_doubled(textBox_Code, textBox_In, textBox_GOST.value, ActiveWorkbook.Worksheets(sheet_name)) = False Then
                Select Case sheet_name
                        Case "arr_SubAssemblies": write_element ActiveWorkbook.Worksheets(sheet_name), num_SubAssemblies
                        Case "arr_Parts": write_element ActiveWorkbook.Worksheets(sheet_name), num_Parts
                End Select
                listbox_update_added sheet_name, textBox_Code, textBox_Name
                clean_the_form
        Else
            If if_SubAssemblyOrPart_Filled(textBox_Code, textBox_Name, textBox_In, textBox_Amount) = False Then
                MsgBox "��������� ��� ����������� ����"
            ElseIf if_In_Assembly_exists(textBox_In, ActiveWorkbook.Worksheets("arr_RootAssemblies")) = False And if_In_Assembly_exists(textBox_In, ActiveWorkbook.Worksheets("arr_SubAssemblies")) = False Then
                MsgBox "������, � ������� ������ ������ �������, �� ����������!"
            ElseIf if_doubled(textBox_Code, textBox_In, textBox_GOST.value, ActiveWorkbook.Worksheets(sheet_name)) = True Then
                MsgBox "������� � ����� ������������ � ���������� ��� ����������!"
            End If
        End If
    End If
      
    If ob_StandartPart.value = True Or ob_Material.value = True Or ob_Others.value = True Then
    
        If ob_StandartPart.value = True Then
            sheet_name = "arr_StandartParts"
            num = num_StandartParts
        ElseIf ob_Material.value = True Then
            sheet_name = "arr_Materials"
            num = num_Materials
         ElseIf ob_Others.value = True Then
            sheet_name = "arr_Others"
            num = num_Others
        End If
    
        If if_StdPartOrMatOrOth_Filled(textBox_Code, textBox_In, textBox_Amount, textBox_GOST) = True And _
           (if_In_Assembly_exists(textBox_In, ActiveWorkbook.Worksheets("arr_RootAssemblies")) = True Or if_In_Assembly_exists(textBox_In, ActiveWorkbook.Worksheets("arr_SubAssemblies")) = True) And _
           if_doubled(textBox_Code, textBox_In, textBox_GOST.value, ActiveWorkbook.Worksheets(sheet_name)) = False Then
                Select Case sheet_name
                    Case "arr_StandartParts": write_element ActiveWorkbook.Worksheets(sheet_name), num_StandartParts
                    Case "arr_Materials": write_element ActiveWorkbook.Worksheets(sheet_name), num_Materials
                    Case "arr_Others": write_element ActiveWorkbook.Worksheets(sheet_name), num_Others
                End Select
                listbox_update_added sheet_name, textBox_Code, textBox_Name
                clean_the_form
        Else
            If if_StdPartOrMatOrOth_Filled(textBox_Code, textBox_In, textBox_Amount, textBox_GOST) = False Then
                MsgBox "��������� ��� ����������� ����"
            ElseIf if_In_Assembly_exists(textBox_In, ActiveWorkbook.Worksheets("arr_RootAssemblies")) = False And if_In_Assembly_exists(textBox_In, ActiveWorkbook.Worksheets("arr_SubAssemblies")) = False Then
                MsgBox "������, � ������� ������ ������ ����������� ������, �� ����������!"
            ElseIf if_doubled(textBox_Code, textBox_In, textBox_GOST.value, ActiveWorkbook.Worksheets(sheet_name)) = True Then
                MsgBox "������� � ����� ������������ � ���������� ��� ����������!"
            End If
        End If
    End If
    
    logs_data = Array(textBox_Code, textBox_Name, "��������� �", textBox_In)
    drop_logs logs_data
    
    Exit Sub
    
instr1:
    MsgBox "������! ���������� ������ �� ������� �������"
    Application.StatusBar = False
    
End Sub

Private Sub cd_Process_Click()
    On Error GoTo instr1
        Application.StatusBar = "���� 1/7. �������� ��������� ��������..."
        bubble_sort ActiveWorkbook.Worksheets("arr_RootAssemblies"), 2
        bubble_sort ActiveWorkbook.Worksheets("arr_SubAssemblies"), 2
        bubble_sort ActiveWorkbook.Worksheets("arr_Parts"), 2
        bubble_sort ActiveWorkbook.Worksheets("arr_StandartParts"), 2
        bubble_sort ActiveWorkbook.Worksheets("arr_Materials"), 2
        bubble_sort ActiveWorkbook.Worksheets("arr_Others"), 2
        
        Application.StatusBar = "���� 3/7. ������ ���������..."
        count_the_sums ActiveWorkbook.Worksheets("arr_SubAssemblies")
        Application.StatusBar = "���� 4/7. ������ ������..."
        count_the_sums ActiveWorkbook.Worksheets("arr_Parts")
        Application.StatusBar = "���� 5/7. ������ ����������� ������..."
        count_the_sums ActiveWorkbook.Worksheets("arr_StandartParts")
        
        Application.StatusBar = "���� 6/7. ������ ���������..."
        count_the_sums_materials ActiveWorkbook.Worksheets("arr_Materials")
        
        Application.StatusBar = "���� 7/7. ������ ������ �������..."
        count_the_sums ActiveWorkbook.Worksheets("arr_Others")
        
        
        Application.StatusBar = "�������� �������..."
        clean_excel_list ActiveWorkbook.Worksheets("�������"), numOfSheets
        numOfSheets = 1
        Dim rowNumber As Long
        rowNumber = 2
    
        With ActiveWorkbook.Worksheets("�������")
        
            If ActiveWorkbook.Worksheets("arr_RootAssemblies").Cells(4, 1) <> "" Then
            With .Range("A" & rowNumber & ":M" & rowNumber)
                .Merge (True)
                .HorizontalAlignment = xlCenter
            End With
            .Cells(rowNumber, 1) = "�������� ������"
            excel_output ActiveWorkbook.Worksheets("arr_RootAssemblies"), rowNumber
            rootAssemblies_maxRow = rowNumber
            End If
            
            If ActiveWorkbook.Worksheets("arr_SubAssemblies").Cells(4, 1) <> "" Then
            With .Range("A" & rowNumber & ":M" & rowNumber)
                .Merge (True)
                .HorizontalAlignment = xlCenter
            End With
            .Cells(rowNumber, 1) = "���������"
            excel_output ActiveWorkbook.Worksheets("arr_SubAssemblies"), rowNumber
            subAssemblies_maxRow = rowNumber
            End If
            
            If ActiveWorkbook.Worksheets("arr_Parts").Cells(4, 1) <> "" Then
            With .Range("A" & rowNumber & ":M" & rowNumber)
                .Merge (True)
                .HorizontalAlignment = xlCenter
            End With
            .Cells(rowNumber, 1) = "������"
            excel_output ActiveWorkbook.Worksheets("arr_Parts"), rowNumber
            parts_maxRow = rowNumber
            End If
            
            If ActiveWorkbook.Worksheets("arr_StandartParts").Cells(4, 1) <> "" Then
            With .Range("A" & rowNumber & ":M" & rowNumber)
                .Merge (True)
                .HorizontalAlignment = xlCenter
            End With
            .Cells(rowNumber, 1) = "����������� �������"
            excel_output ActiveWorkbook.Worksheets("arr_StandartParts"), rowNumber
            standartParts_maxRow = rowNumber
            End If
            
            If ActiveWorkbook.Worksheets("arr_Materials").Cells(4, 1) <> "" Then
            With .Range("A" & rowNumber & ":M" & rowNumber)
                .Merge (True)
                .HorizontalAlignment = xlCenter
            End With
            .Cells(rowNumber, 1) = "���������"
            excel_output ActiveWorkbook.Worksheets("arr_Materials"), rowNumber
            materials_maxRow = rowNumber
            End If
            
            If ActiveWorkbook.Worksheets("arr_Others").Cells(4, 1) <> "" Then
            With .Range("A" & rowNumber & ":M" & rowNumber)
                .Merge (True)
                .HorizontalAlignment = xlCenter
            End With
            .Cells(rowNumber, 1) = "������ �������"
            excel_output ActiveWorkbook.Worksheets("arr_Others"), rowNumber
            others_maxRow = rowNumber
            End If
        End With
            
        enumerate
        expanded_strings_num = 0
        Application.StatusBar = "������!"
        Application.StatusBar = False
            
        logs_data = Array("��������� ���������")
        drop_logs logs_data
        Exit Sub
        
instr1:
    MsgBox "������! ���������� ������ �� ������� �������"
    Application.StatusBar = False
End Sub


Private Sub checkBox_nodwg_Change()
    Select Case mainModule.obj_addItemForm.checkBox_noDwg.value
        Case True:  mainModule.obj_noDwgSideForm.Show
                    mainModule.obj_noDwgSideForm.Top = mainModule.obj_addItemForm.Top
                    mainModule.obj_noDwgSideForm.Left = mainModule.obj_addItemForm.Left + mainModule.obj_addItemForm.Width
                    
                    
        Case False: mainModule.obj_noDwgSideForm.Hide
    End Select
End Sub

Private Sub checkBox_Painted_Click()

End Sub

Private Sub checkBox_toBuy_Change()
    If mainModule.obj_addItemForm.checkBox_toBuy.value = True Then mainModule.obj_addItemForm.checkBox_toCoop.value = True
End Sub


Private Sub CommandButton10_Click()
    mainModule.obj_addItemForm.checkBox_noDwg.value = False
    cd_Process_Click
    mainModule.obj_addItemForm.Hide
    mainModule.obj_propInputForm.Show
End Sub

Private Sub CommandButton7_Click()  '������ �� �������
    clean_excel_list ActiveWorkbook.Worksheets("�������"), numOfSheets
    numOfSheets = 1
    Dim rowNumber As Long
    rowNumber = 2
    With ActiveWorkbook.Worksheets("�������")
        With .Range("A" & rowNumber & ":M" & rowNumber)
            .Merge (True)
            .HorizontalAlignment = xlCenter
        End With
        .Cells(rowNumber, 1) = "�������� ������"
        excel_list_update ActiveWorkbook.Worksheets("arr_RootAssemblies"), rowNumber
        
        rootAssemblies_maxRow = rowNumber
        
        With .Range("A" & rowNumber & ":M" & rowNumber)
            .Merge (True)
            .HorizontalAlignment = xlCenter
        End With
        .Cells(rowNumber, 1) = "���������"
        excel_list_update ActiveWorkbook.Worksheets("arr_SubAssemblies"), rowNumber
        
        subAssemblies_maxRow = rowNumber
        
        With .Range("A" & rowNumber & ":M" & rowNumber)
            .Merge (True)
            .HorizontalAlignment = xlCenter
        End With
        .Cells(rowNumber, 1) = "������"
        excel_list_update ActiveWorkbook.Worksheets("arr_Parts"), rowNumber
        
        parts_maxRow = rowNumber
        
        With .Range("A" & rowNumber & ":M" & rowNumber)
            .Merge (True)
            .HorizontalAlignment = xlCenter
        End With
        .Cells(rowNumber, 1) = "����������� �������"
        excel_list_update ActiveWorkbook.Worksheets("arr_StandartParts"), rowNumber
        
        standartParts_maxRow = rowNumber
        
        With .Range("A" & rowNumber & ":M" & rowNumber)
            .Merge (True)
            .HorizontalAlignment = xlCenter
        End With
        .Cells(rowNumber, 1) = "���������"
        excel_list_update ActiveWorkbook.Worksheets("arr_Materials"), rowNumber
        
        materials_maxRow = rowNumber
        
        With .Range("A" & rowNumber & ":M" & rowNumber)
            .Merge (True)
            .HorizontalAlignment = xlCenter
        End With
        .Cells(rowNumber, 1) = "������ �������"
        excel_list_update ActiveWorkbook.Worksheets("arr_Others"), rowNumber
        
        others_maxRow = rowNumber
    End With
End Sub

Private Sub CommandButton8_Click()  ' ������ �������� / �������
    mainModule.obj_addItemForm.checkBox_noDwg.value = False
    cd_Process_Click            ' ����-������� ������ "���������"
    mainModule.obj_addItemForm.Hide
    mainModule.obj_editDeleteForm.Show
End Sub

Private Sub CommandButton9_Click()
    logs_data = Array("������ ���������")
    drop_logs logs_data
    ActiveWorkbook.Close
End Sub

Private Sub Frame2_Click()

End Sub

Private Sub ob_Material_Click()         '����� ���������
    select_Material
    clean_the_form
End Sub

Private Sub ob_Others_Click()           ' ����� ������ �������
    select_Others
    clean_the_form
End Sub

Private Sub ob_Part_Click()             '����� ������
    select_Part
    clean_the_form
End Sub

Private Sub ob_RootAssembly_Click()     '����� �������� ������
    select_RootAssembly
    clean_the_form
End Sub

Private Sub ob_StandartPart_Click()     '����� ����������� ������
    select_StandartPart
    clean_the_form
End Sub

Private Sub ob_SubAssembly_Click()      '����� ���������
    select_SubAssembly
    clean_the_form
End Sub

Private Sub OptionButton1_Click()
    select_Others
    clean_the_form
End Sub

Private Sub textBox_Amount_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    check_format_amount KeyAscii, mainModule.obj_addItemForm.textBox_Amount
End Sub

Private Sub textBox_Code_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)  '�������� ����� � ���������
   If mainModule.obj_addItemForm.ob_RootAssembly = True Or _
      mainModule.obj_addItemForm.ob_SubAssembly = True Or _
      mainModule.obj_addItemForm.ob_Part = True Then _
      check_format_num KeyAscii, mainModule.obj_addItemForm.textBox_Code    '��� ������� �����, ����� �������������� ���� ������ ��� ������ � �������
                                                                            ' ��� ���������� � ����������� ������� �������� ����� ��������������� �� ��������
End Sub

Private Sub textBox_GOST_Change()

End Sub

Private Sub textBox_In_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    check_format_num KeyAscii, mainModule.obj_addItemForm.textBox_In
End Sub

Private Sub UserForm_Activate()         '����������� ����� �������� ������
    select_Part
   ' ListBox1.Clear
    ListBox1.Enabled = False
End Sub

Private Sub UserForm_QueryClose(Cancel As Integer, CloseMode As Integer)
    If CloseMode = 0 Then Cancel = 1
End Sub
