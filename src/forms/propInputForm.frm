VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} propInputForm 
   Caption         =   "������� ��������������� ���������� �� ��������:"
   ClientHeight    =   7770
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   14835
   OleObjectBlob   =   "propInputForm.frx":0000
   ShowModal       =   0   'False
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "propInputForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cb_bar_Change()
    Select Case mainModule.obj_propInputForm.cb_bar.ListIndex
        Case 0: select_0 Me   '����/������
        Case 1: select_1 Me   '����
        Case 2: select_2 Me   '������������
        Case 3: select_3 Me   '������
        Case 4: select_4 Me   '����� �������
        Case 5: select_5 Me   '����� �������������
        Case 6: select_6 Me   '�������
    End Select
End Sub


Private Sub cb_bill_Click()
    integrate_materials
 '   create_report
End Sub

Private Sub cb_deletePaint_Click()
    With mainModule.obj_propInputForm
        delete_paint .ListBox2.List(.ListBox2.ListIndex, 1), .ListBox2.List(.ListBox2.ListIndex, 0), .textBox_Code
        get_paint_list .textBox_Code
        If .ListBox2.ListCount > 0 Then
            .ListBox2.ListIndex = 0
            Else: .ListBox2.ListIndex = -1
        End If
        If .ListBox2.ListCount = 0 Then
            set_element_painted ActiveWorkbook.Worksheets("arr_Parts"), .textBox_Code, 0
            .cb_TT = False
            .cb_paint = ""
            .cb_paintGOST = ""
            .textBox_Cons = ""
            .textBox_ConsCoeff = ""
            .textBox_Area = ""
        End If
    End With
End Sub

Private Sub cb_exit_Click()
    mainModule.obj_propInputForm.Hide
    mainModule.obj_addItemForm.Show
End Sub

Private Sub cb_input_Click()

    On Error GoTo instr
    If if_propInfo_filled Then
    
        write_propInfo Me, selected_part_index
         
        If mainModule.obj_propInputForm.ListBox1.ListIndex <= mainModule.obj_propInputForm.ListBox1.ListCount - 2 Then
            listbox_update mainModule.obj_propInputForm.ListBox1.ListIndex + 1
        Else: listbox_update mainModule.obj_propInputForm.ListBox1.ListIndex
        End If
        
        lable_update
        tips_update Me
        tips_update_paint Me
    
    Else: MsgBox "������� ��� ����������� ������!"
    End If
    Exit Sub
instr:
   MsgBox "��������� ������������ �����. ������: " & Err.Description

End Sub

Private Sub cb_paint_Change()
 Dim flag As Boolean
    
    flag = check_if_complex(cb_paint)
    
        With mainModule.obj_propInputForm
    If flag = True Then
            .cb_paintGOST.Enabled = False
            .textBox_layersNum.Enabled = False
            .textBox_Cons.Enabled = False
            .textBox_ConsCoeff.Enabled = False
            .textBox_Area.Enabled = True
    Else
            .cb_paintGOST.Enabled = True
            .textBox_layersNum.Enabled = True
            .textBox_Cons.Enabled = True
            .textBox_ConsCoeff.Enabled = True
            .textBox_Area.Enabled = True
    End If
    End With
End Sub

Private Sub cb_paint_Enter()
   
    
End Sub

Private Sub cb_setPaint_Click()

    On Error GoTo instr

'       �������� �������� �� �������� ����� ������ �����
    Dim sheet_name, buffer_paint As String
    Dim num As Long
    Dim index As Long
    
    sheet_name = "arr_Materials"
    num = num_Materials
    
    Select Case textBox_layersNum.Enabled
        
        Case True:                '���� ��������� �������, ������ �������� ��������� ��������
             If if_paint_Filled(cb_paint, cb_paintGOST, textBox_Cons, textBox_ConsCoeff, textBox_Area, textBox_layersNum) = True And _
                if_doubled(cb_paint, textBox_Code, cb_paintGOST.value, ActiveWorkbook.Worksheets(sheet_name)) = False Then
                     write_element_paint ActiveWorkbook.Worksheets(sheet_name), num_Materials
                     set_element_painted ActiveWorkbook.Worksheets("arr_Parts"), mainModule.obj_propInputForm.textBox_Code, 1
                    ' listbox_update_added sheet_name, textBox_Code, textBox_Name
                    ' clean_the_paint_form - �� ��������
                    get_paint_list mainModule.obj_propInputForm.textBox_Code
            Else
            If if_paint_Filled(cb_paint, cb_paintGOST, textBox_Cons, textBox_ConsCoeff, textBox_Area, textBox_layersNum) = False Then
                MsgBox "��������� ��� ����������� ����"
            ElseIf if_doubled(cb_paint, textBox_Code, cb_paintGOST.value, ActiveWorkbook.Worksheets(sheet_name)) = True Then
                MsgBox "������� � ����� ������������ � ���������� ��� ����������!"
            End If
            End If
    
        Case False:             '���� �������� ���������, ������ �������� ����������� �������� �� db_paint
            If textBox_Area <> "" And cb_paint <> "" Then
                index = 4
                buffer_paint = cb_paint
                    With ThisWorkbook.Worksheets("db_paint")
                    Do While .Cells(index, 1) <> ""
                        If .Cells(index, 2) = buffer_paint Then
                            write_element_paint_complex ActiveWorkbook.Worksheets(sheet_name), num_Materials, _
                                .Cells(index, 3), .Cells(index, 4), textBox_Code, textBox_Area, .Cells(index, 6), .Cells(index, 7), .Cells(index, 5), .Cells(index, 8)
                            set_element_painted ActiveWorkbook.Worksheets("arr_Parts"), mainModule.obj_propInputForm.textBox_Code, 1
                        End If
                        index = index + 1
                    Loop
                    End With
                    mainModule.obj_propInputForm.ListBox2.Clear
                    get_paint_list mainModule.obj_propInputForm.textBox_Code
            ElseIf if_paint_Filled(cb_paint, cb_paintGOST, textBox_Cons, textBox_ConsCoeff, textBox_Area, textBox_layersNum) = False Then
                MsgBox "��������� ��� ����������� ����"
            End If
    End Select
    
   Exit Sub
   
instr:
   MsgBox "���������� ������������ �����. ������: " & Err.Description
    
End Sub

Private Sub cb_TT_Change()
    With mainModule.obj_propInputForm
        Select Case .cb_TT.value
            Case True:  .textBox_Area.Enabled = True
                        .cb_paint.Enabled = True
                        .cb_paintGOST.Enabled = True
                        .textBox_Cons.Enabled = True
                        .textBox_ConsCoeff.Enabled = True
                        .cb_setPaint.Enabled = True
                        .cb_deletePaint.Enabled = True
                        If .ListBox2.ListCount > 0 Then .cb_TT.Enabled = False
                                                
            Case False: .textBox_Area.Enabled = False
                        .cb_paint.Enabled = False
                        .cb_paintGOST.Enabled = False
                        .textBox_Cons.Enabled = False
                        .textBox_ConsCoeff.Enabled = False
                        .cb_setPaint.Enabled = False
                        .cb_deletePaint.Enabled = False
                        .cb_TT.Enabled = True
        End Select
    End With
End Sub

Private Sub cb_userbar_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ListBox1.ListIndex > 0 Then display_recalled_item ListBox1.List(ListBox1.ListIndex - 1)
End Sub

Private Sub cd_Process_Click()
    On Error GoTo instr1

    Application.StatusBar = "���� 1/7. �������� ��������� ��������..."
    bubble_sort ActiveWorkbook.Worksheets("arr_RootAssemblies"), 2
    bubble_sort ActiveWorkbook.Worksheets("arr_SubAssemblies"), 2
    bubble_sort ActiveWorkbook.Worksheets("arr_Parts"), 2
    bubble_sort ActiveWorkbook.Worksheets("arr_StandartParts"), 2
    bubble_sort ActiveWorkbook.Worksheets("arr_Materials"), 2
    bubble_sort ActiveWorkbook.Worksheets("arr_Others"), 2
    
    Application.StatusBar = "���� 3/7. ������ ���������..."
    count_the_sums ActiveWorkbook.Worksheets("arr_SubAssemblies")
    Application.StatusBar = "���� 4/7. ������ ������..."
    count_the_sums ActiveWorkbook.Worksheets("arr_Parts")
    Application.StatusBar = "���� 5/7. ������ ����������� ������..."
    count_the_sums ActiveWorkbook.Worksheets("arr_StandartParts")
    Application.StatusBar = "���� 6/7. ������ ���������..."
    count_the_sums_materials ActiveWorkbook.Worksheets("arr_Materials")
    Application.StatusBar = "���� 7/7. ������ ������ �������..."
    count_the_sums ActiveWorkbook.Worksheets("arr_Others")
    
    
    Application.StatusBar = "�������� �������..."
    clean_excel_list ActiveWorkbook.Worksheets("�������"), numOfSheets
    numOfSheets = 1
    Dim rowNumber As Long
    rowNumber = 2
    
    With ActiveWorkbook.Worksheets("�������")
    
        If ActiveWorkbook.Worksheets("arr_RootAssemblies").Cells(4, 1) <> "" Then
        With .Range("A" & rowNumber & ":M" & rowNumber)
            .Merge (True)
            .HorizontalAlignment = xlCenter
        End With
        .Cells(rowNumber, 1) = "�������� ������"
        excel_output ActiveWorkbook.Worksheets("arr_RootAssemblies"), rowNumber
        rootAssemblies_maxRow = rowNumber
        End If
        
        If ActiveWorkbook.Worksheets("arr_SubAssemblies").Cells(4, 1) <> "" Then
        With .Range("A" & rowNumber & ":M" & rowNumber)
            .Merge (True)
            .HorizontalAlignment = xlCenter
        End With
        .Cells(rowNumber, 1) = "���������"
        excel_output ActiveWorkbook.Worksheets("arr_SubAssemblies"), rowNumber
        subAssemblies_maxRow = rowNumber
        End If
        
        If ActiveWorkbook.Worksheets("arr_Parts").Cells(4, 1) <> "" Then
        With .Range("A" & rowNumber & ":M" & rowNumber)
            .Merge (True)
            .HorizontalAlignment = xlCenter
        End With
        .Cells(rowNumber, 1) = "������"
        excel_output ActiveWorkbook.Worksheets("arr_Parts"), rowNumber
        parts_maxRow = rowNumber
        End If
        
        If ActiveWorkbook.Worksheets("arr_StandartParts").Cells(4, 1) <> "" Then
        With .Range("A" & rowNumber & ":M" & rowNumber)
            .Merge (True)
            .HorizontalAlignment = xlCenter
        End With
        .Cells(rowNumber, 1) = "����������� �������"
        excel_output ActiveWorkbook.Worksheets("arr_StandartParts"), rowNumber
        standartParts_maxRow = rowNumber
        End If
        
        If ActiveWorkbook.Worksheets("arr_Materials").Cells(4, 1) <> "" Then
        With .Range("A" & rowNumber & ":M" & rowNumber)
            .Merge (True)
            .HorizontalAlignment = xlCenter
        End With
        .Cells(rowNumber, 1) = "���������"
        excel_output ActiveWorkbook.Worksheets("arr_Materials"), rowNumber
        materials_maxRow = rowNumber
        End If
        
        If ActiveWorkbook.Worksheets("arr_Others").Cells(4, 1) <> "" Then
        With .Range("A" & rowNumber & ":M" & rowNumber)
            .Merge (True)
            .HorizontalAlignment = xlCenter
        End With
        .Cells(rowNumber, 1) = "������ �������"
        excel_output ActiveWorkbook.Worksheets("arr_Others"), rowNumber
        others_maxRow = rowNumber
        End If
    End With
        
    enumerate
    expanded_strings_num = 0
    Application.StatusBar = "������!"
    Application.StatusBar = False
        
    logs_data = Array("��������� ���������")
    drop_logs logs_data
    Exit Sub
        
instr1:
    MsgBox "������! ���������� ������ �� ������� �������"
    Application.StatusBar = False
    
End Sub

Private Sub checkBox_toCoop_Change()
    Select Case mainModule.obj_propInputForm.checkBox_toCoop.value
        Case True:  mainModule.obj_propInputForm.textBox_coop_def.Enabled = True
        Case False: mainModule.obj_propInputForm.textBox_coop_def.Enabled = False
                    mainModule.obj_propInputForm.textBox_coop_def = ""
    End Select
End Sub

Private Sub Label10_Click()
    mainModule.obj_propInputForm.Hide
    mainModule.obj_densityUpdateForm.Show
End Sub

Private Sub ListBox1_Click()
    display_item mainModule.obj_propInputForm.ListBox1.List(mainModule.obj_propInputForm.ListBox1.ListIndex)
End Sub

Private Sub ListBox2_Click()
    display_paint 'mainModule.obj_propInputForm.ListBox2.Value
End Sub

Private Sub MultiPage1_Change()

End Sub

Private Sub textBox_A_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    check_format_amount KeyAscii, mainModule.obj_propInputForm.textBox_A
End Sub

Private Sub textBox_B_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    check_format_amount KeyAscii, mainModule.obj_propInputForm.textBox_B
End Sub

Private Sub textBox_C_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    check_format_amount KeyAscii, mainModule.obj_propInputForm.textBox_C
End Sub

Private Sub textBox_D_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    check_format_amount KeyAscii, mainModule.obj_propInputForm.textBox_D
End Sub

Private Sub textBox_ExactWeight_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
    check_format_amount KeyAscii, mainModule.obj_propInputForm.textBox_ExactWeight
End Sub

Private Sub UserForm_Activate()
       
    fill_db_density db_density
           
    cb_density_display Me
    cb_bar_display Me
    
    tips_update Me
    tips_update_paint Me
    
    listbox_update 0
    
    lable_update
    
    If mainModule.obj_propInputForm.checkBox_toCoop.value = True Then
        mainModule.obj_propInputForm.textBox_coop_def.Enabled = True
    Else: mainModule.obj_propInputForm.textBox_coop_def.Enabled = False
    End If
    
    
    mainModule.obj_propInputForm.cb_deletePaint.Enabled = False
End Sub

Private Sub UserForm_QueryClose(Cancel As Integer, CloseMode As Integer)
If CloseMode = 0 Then Cancel = 1
End Sub
