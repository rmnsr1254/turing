Attribute VB_Name = "editDeleteModule"
Option Explicit

Public row_to_edit_index As Long          ' ������ ���������� �������� � ����
Public sheet_to_edit_from_name As String   ' ������ �����, ����������� �������� �������

Public Sub edit_sub()                       '������� ��������� �������������� ���������
On Error GoTo instr1
    clean_edit_form
    With mainModule.obj_editDeleteForm
        .CommandButton1.Enabled = False
        .CommandButton2.Enabled = False
    End With
    
    Dim elemRowNumber As Long              ' ����� ��������� ������ �� �������
         
    elemRowNumber = ActiveCell.row
    
    If ActiveWorkbook.Worksheets("�������").Cells(elemRowNumber, 4) = "" Then
        MsgBox "�� ������� ���������� �������!"
        With mainModule.obj_editDeleteForm
            .CommandButton1.Enabled = False
            .CommandButton2.Enabled = False
        End With
    Else
        With ActiveWorkbook.Worksheets("�������")
            find_element_to_edit .Cells(elemRowNumber, 1), .Cells(elemRowNumber, 3), .Cells(elemRowNumber, 13), row_to_edit_index, sheet_to_edit_from_name
        End With
        
        If row_to_edit_index = 0 Then
            MsgBox "�� ������� ���������� �������!"
            With mainModule.obj_editDeleteForm
                .CommandButton1.Enabled = False
                .CommandButton2.Enabled = False
            End With
        Else
            With mainModule.obj_editDeleteForm
                .CommandButton1.Enabled = True
                .CommandButton2.Enabled = True
            End With
            display_element_to_edit row_to_edit_index, sheet_to_edit_from_name
        End If
    End If
    Exit Sub
    
instr1:
    MsgBox "������! ���������� ������ �� ������� �������"
    Application.StatusBar = False
End Sub

Public Sub find_element_to_edit(ByVal code, is_in, gost As String, ByRef row_to_edit_index, ByRef sheet_to_edit_from_name As String)
    ' �������� � sheet_to_edit_from_name ������ �����, ����������� �������� ������� � ��� ������ � ��� � row_to_edit_index
    
    Dim i, j As Long
    'j - ������ �����, i - ������ ������ � �����
    
    For j = 2 To ActiveWorkbook.Worksheets.count
    i = 4
        Do While ActiveWorkbook.Worksheets(j).Cells(i, 1) <> ""
            If ActiveWorkbook.Worksheets(j).name = "arr_RootAssemblies" Or _
                      ActiveWorkbook.Worksheets(j).name = "arr_SubAssemblies" Or _
                        ActiveWorkbook.Worksheets(j).name = "arr_Parts" Then
                If ActiveWorkbook.Worksheets(j).Cells(i, 2) = code And _
                    ActiveWorkbook.Worksheets(j).Cells(i, 4) = is_in Then
                        row_to_edit_index = i
                        sheet_to_edit_from_name = ActiveWorkbook.Worksheets(j).name
                        Exit Sub
                End If
             ElseIf ActiveWorkbook.Worksheets(j).name = "arr_StandartParts" Or _
                      ActiveWorkbook.Worksheets(j).name = "arr_Materials" Or _
                        ActiveWorkbook.Worksheets(j).name = "arr_Others" Then
                If ActiveWorkbook.Worksheets(j).Cells(i, 2) = code And _
                    ActiveWorkbook.Worksheets(j).Cells(i, 4) = is_in And _
                        ActiveWorkbook.Worksheets(j).Cells(i, 8) = gost Then
                        row_to_edit_index = i
                        sheet_to_edit_from_name = ActiveWorkbook.Worksheets(j).name
                        Exit Sub
                End If
        End If
        i = i + 1
        Loop
    Next j
    
    row_to_edit_index = 0
    
End Sub

Public Sub display_element_to_edit(ByVal row_index As Long, ByVal sheet_name As String)
        With mainModule.obj_editDeleteForm
            
            .textBox_Code.Enabled = True
            .textBox_Name.Enabled = True
            .textBox_Amount.Enabled = True
            .textBox_In.Enabled = True
            .textBox_GOST.Enabled = True
                        
            .textBox_Code = CStr(ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 2))
            .textBox_Name = CStr(ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 3))
            .textBox_In = CStr(ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 4))
            .textBox_Amount = CStr(ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 5))
            .textBox_GOST = CStr(ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 8))
            .cb_Units = CStr(ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 21))
            
            .checkBox_toBuy.value = CBool(ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 24))
            .checkBox_toCoop.value = CBool(ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 25))
            .checkBox_isPainted.value = CBool(ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 28))
            
            If sheet_name = "arr_Materials" Then
                .cb_Units.Enabled = True
            Else
                .cb_Units.Enabled = False
            End If
            
            logs_data = Array(.textBox_Code, .textBox_Name, "�������� �", .textBox_In, .textBox_Amount, .textBox_GOST, "��������� ��� ���������������")
            drop_logs logs_data
            
        End With
        
        
End Sub

Public Sub save_after_edit_sub()
' ��������� �������� � ���������� ��������� ������
On Error GoTo instr1
With mainModule.obj_editDeleteForm
    
    If sheet_to_edit_from_name = "arr_RootAssemblies" Then        ' �������� ��� ����� �������� ������
            If if_RootAssembly_Filled(.textBox_Code.value, .textBox_Name, .textBox_Amount) = True Then
                    save_edited_element sheet_to_edit_from_name, row_to_edit_index
                    listbox_update_edited sheet_to_edit_from_name, .textBox_Code, .textBox_Name
                    clean_edit_form
            Else
               MsgBox "��������� ��� ����������� ����"
            End If
    End If
    
    If sheet_to_edit_from_name = "arr_SubAssemblies" _
        Or sheet_to_edit_from_name = "arr_Parts" Then    '�������� ��� ����� ��������� ��� ������
        
        If if_SubAssemblyOrPart_Filled(.textBox_Code, .textBox_Name, .textBox_In, .textBox_Amount) = True And _
           (if_In_Assembly_exists(.textBox_In, ActiveWorkbook.Worksheets("arr_RootAssemblies")) = True Or _
            if_In_Assembly_exists(.textBox_In, ActiveWorkbook.Worksheets("arr_SubAssemblies")) = True) Then
                save_edited_element sheet_to_edit_from_name, row_to_edit_index
                listbox_update_edited sheet_to_edit_from_name, .textBox_Code, .textBox_Name
                clean_edit_form
        Else
            If if_SubAssemblyOrPart_Filled(.textBox_Code, .textBox_Name, .textBox_In, .textBox_Amount) = False Then
                MsgBox "��������� ��� ����������� ����"
            ElseIf if_In_Assembly_exists(.textBox_In, ActiveWorkbook.Worksheets("arr_RootAssemblies")) = False _
                        And if_In_Assembly_exists(.textBox_In, ActiveWorkbook.Worksheets("arr_SubAssemblies")) = False Then _
                MsgBox "������, � ������� ������ ������ �������, �� ����������!"
            End If
       End If
    End If

   If sheet_to_edit_from_name = "arr_StandartParts" _
        Or sheet_to_edit_from_name = "arr_Materials" _
        Or sheet_to_edit_from_name = "arr_Others" Then            '�������� ��� ����� ����������� ������, ��������� ��� ������� �������
        
        If if_StdPartOrMatOrOth_Filled(.textBox_Code, .textBox_In, .textBox_Amount, .textBox_GOST) = True And _
           (if_In_Assembly_exists(.textBox_In, ActiveWorkbook.Worksheets("arr_RootAssemblies")) = True Or _
            if_In_Assembly_exists(.textBox_In, ActiveWorkbook.Worksheets("arr_SubAssemblies")) = True) Then
                save_edited_element sheet_to_edit_from_name, row_to_edit_index
                listbox_update_edited sheet_to_edit_from_name, .textBox_Code, .textBox_Name
                clean_edit_form
        Else
            If if_StdPartOrMatOrOth_Filled(.textBox_Code, .textBox_In, .textBox_Amount, .textBox_GOST) = False Then
                MsgBox "��������� ��� ����������� ����"
            ElseIf if_In_Assembly_exists(.textBox_In, ActiveWorkbook.Worksheets("arr_RootAssemblies")) = False And if_In_Assembly_exists(.textBox_In, ActiveWorkbook.Worksheets("arr_SubAssemblies")) = False Then
                MsgBox "������, � ������� ������ ������ �������, �� ����������!"
            End If
        End If
    End If
End With
Exit Sub
    
instr1:
    MsgBox "������! ���������� ������ �� ������� �������"
    Application.StatusBar = False
End Sub
    
Public Sub listbox_update_edited(ByVal sheet_name As String, ByVal code, name As String)
    '���������� ��������� ����� ��������������
    With mainModule.obj_editDeleteForm.ListBox1
        Select Case sheet_name
            Case "arr_RootAssemblies": .AddItem (Date & " " & Time() & " �������� ������ '" & code & " - " & name & "' ��������� �������!")
            Case "arr_SubAssemblies": .AddItem (Date & " " & Time() & " ��������� '" & code & " - " & name & "' ��������� �������!")
            Case "arr_Parts": .AddItem (Date & " " & Time() & " ������ '" & code & " - " & name & "' ��������� �������!")
            Case "arr_StandartParts": .AddItem (Date & " " & Time() & " ����������� ������ '" & code & " - " & name & "' ��������� �������!")
            Case "arr_Materials": .AddItem (Date & " " & Time() & " �������� '" & code & " - " & name & "' �������� �������!")
            Case "arr_Others": .AddItem (Date & " " & Time() & " ������ ������� '" & code & " - " & name & "' ��������� �������!")
        End Select
            .ListIndex = .ListCount - 1
    End With
End Sub

Public Sub listbox_update_deleted(ByVal sheet_name As String, ByVal code, name As String)
    '���������� ��������� ����� ��������
     With mainModule.obj_editDeleteForm.ListBox1
        Select Case sheet_name
            Case "arr_RootAssemblies": .AddItem (Date & " " & Time() & " �������� ������ '" & code & " - " & name & "'� ��� �������� � ��� �������� �������!")
            Case "arr_SubAssemblies": .AddItem (Date & " " & Time() & " ��������� '" & code & " - " & name & "'� ��� �������� � ��� �������� �������!")
            Case "arr_Parts": .AddItem (Date & " " & Time() & " ������ '" & code & " - " & name & "' �������!")
            Case "arr_StandartParts": .AddItem (Date & " " & Time() & " ����������� ������ '" & code & " - " & name & "' �������!")
            Case "arr_Materials": .AddItem (Date & " " & Time() & " �������� '" & code & " - " & name & "' ������!")
            Case "arr_Others": .AddItem (Date & " " & Time() & " ������ ������� '" & code & " - " & name & "' �������!")
        End Select
            .ListIndex = .ListCount - 1
    End With
End Sub

Public Sub save_edited_element(ByVal sheet_name As String, row_index As Long)
' ������ � ���� ����� �����
Dim i As Long
    With mainModule.obj_editDeleteForm
        ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 1) = Date & " " & Time()
        
        ActiveWorkbook.Worksheets("arr_subAssemblies").Columns("D").Replace ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 2), .textBox_Code, 2
        ActiveWorkbook.Worksheets("arr_Parts").Columns("D").Replace ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 2), .textBox_Code, 2
        ActiveWorkbook.Worksheets("arr_StandartParts").Columns("D").Replace ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 2), .textBox_Code, 2
        ActiveWorkbook.Worksheets("arr_Materials").Columns("D").Replace ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 2), .textBox_Code, 2
        ActiveWorkbook.Worksheets("arr_Others").Columns("D").Replace ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 2), .textBox_Code, 2
        
        ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 2) = .textBox_Code
        ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 3) = .textBox_Name
        ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 4) = .textBox_In
        ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 5) = .textBox_Amount
        ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 8) = .textBox_GOST
        ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 21) = .cb_Units
        ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 24) = CLng(.checkBox_toBuy) * (-1)
        Select Case CLng(ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 24))
            Case 0: ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 23) = 0    '��� ��������� ������ � ������� ������ ��� ����� ����� ���� - ���������� ��� �� �������
                                                                                        '�� ���������� �� ���������, ��������� ���� ������������� ������, ������� ��� ������� � �������
                                                                                        '� �� ������ �� ������� �� ���������, �� ���� ������ �������� ��� ������ case � ���������� ���� ���������
            Case 1:
                    ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 23) = 0         '��� ��������� ������ � "�� ��������" - ������ ����������, ���������� � ��������� ���������
                    ActiveWorkbook.Worksheets(sheet_name).Range("H" & CStr(row_index) & ":V" & CStr(row_index)) = ""
        End Select
        ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 25) = CLng(.checkBox_toCoop) * (-1)
        ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 28) = CLng(.checkBox_isPainted) * (-1)
        
        If sheet_name = "arr_RootAssemblies" Or sheet_name = "arr_SubAssemblies" Then
            
            paint_all_included_into ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 2), ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 28)
        End If
        
        logs_data = Array(.textBox_Code, .textBox_Name, "�������� �", .textBox_In, .textBox_Amount, .textBox_GOST, "��������� ����� ��������������")
        drop_logs logs_data
    End With
End Sub

Public Sub paint_all_included_into(ByVal is_in As String, value As Long)
    Dim i, j As Long
    Dim flag As Boolean
    
    flag = False '������������, ��� �� ����� ����������� �����
    If num_SubAssemblies > 0 Then
        For i = 4 To num_SubAssemblies + 3
            If ActiveWorkbook.Worksheets("arr_SubAssemblies").Cells(i, 4) = is_in Then
                flag = True
                ActiveWorkbook.Worksheets("arr_SubAssemblies").Cells(i, 28) = value
                paint_all_included_into ActiveWorkbook.Worksheets("arr_SubAssemblies").Cells(i, 2), value
            End If
          Next i
            For j = 4 To num_Parts + 3
            If ActiveWorkbook.Worksheets("arr_Parts").Cells(j, 4) = is_in Then
                ActiveWorkbook.Worksheets("arr_Parts").Cells(j, 28) = value
                ActiveWorkbook.Worksheets("arr_Parts").Cells(j, 23) = 0
            End If
            Next j
        
    Else
        For j = 4 To num_Parts + 3
            If ActiveWorkbook.Worksheets("arr_Parts").Cells(j, 4) = is_in Then
                ActiveWorkbook.Worksheets("arr_Parts").Cells(j, 28) = value
                ActiveWorkbook.Worksheets("arr_Parts").Cells(j, 23) = 0
            End If
            Next j
    End If
End Sub

Public Sub delete_sub()
On Error GoTo instr1
    Select Case sheet_to_edit_from_name
            Case "arr_RootAssemblies":
                    If MsgBox("�������� �������� ������ �������� � �������� ���� �������� � ��� ���������! ����������?", 1) = vbOK Then
                        deep_clean sheet_to_edit_from_name, row_to_edit_index, num_RootAssemblies
                        With mainModule.obj_editDeleteForm
                        listbox_update_deleted sheet_to_edit_from_name, .textBox_Code, .textBox_Name
                        End With
                    End If
                    
            Case "arr_SubAssemblies":
                    If MsgBox("�������� ��������� �������� � �������� ���� �������� � ��� ���������! ����������?", 1) = vbOK Then
                        deep_clean sheet_to_edit_from_name, row_to_edit_index, num_SubAssemblies
                        With mainModule.obj_editDeleteForm
                        listbox_update_deleted sheet_to_edit_from_name, .textBox_Code, .textBox_Name
                        End With
                    End If
                    
            Case "arr_Parts": ActiveWorkbook.Worksheets(sheet_to_edit_from_name).Rows(row_to_edit_index).Delete
                    num_Parts = num_Parts - 1
            Case "arr_StandartParts": ActiveWorkbook.Worksheets(sheet_to_edit_from_name).Rows(row_to_edit_index).Delete
                    num_StandartParts = num_StandartParts - 1
            Case "arr_Materials": ActiveWorkbook.Worksheets(sheet_to_edit_from_name).Rows(row_to_edit_index).Delete
                    num_Materials = num_Materials - 1
            Case "arr_Others": ActiveWorkbook.Worksheets(sheet_to_edit_from_name).Rows(row_to_edit_index).Delete
                    num_Others = num_Others - 1
    End Select
    
                With mainModule.obj_editDeleteForm
                    listbox_update_deleted sheet_to_edit_from_name, .textBox_Code, .textBox_Name
                    logs_data = Array(.textBox_Code, .textBox_Name, "�������")
                    drop_logs logs_data
                End With
   Exit Sub
    
instr1:
    MsgBox "������! ���������� ������ �� ������� �������"
    Application.StatusBar = False
            
End Sub

Public Sub deep_clean(ByVal sheet_name As String, ByVal row_index As Long, ByRef num As Long)
    ActiveWorkbook.Worksheets(sheet_name).Cells(row_index, 5) = 0
    ' ������ ���������� ���������� �������� ������ ���� � ������������� ��� ��������� �������� ����� ����� ������� ��, ������� ���� ����� ��������
    
    count_the_sums ActiveWorkbook.Worksheets("arr_SubAssemblies")
    count_the_sums ActiveWorkbook.Worksheets("arr_Parts")
    count_the_sums ActiveWorkbook.Worksheets("arr_StandartParts")
    count_the_sums ActiveWorkbook.Worksheets("arr_Materials")
    count_the_sums ActiveWorkbook.Worksheets("arr_Others")
    
    ActiveWorkbook.Worksheets(sheet_name).Rows(row_index).Delete
    num = num - 1
    delete_zeros
    
End Sub

Public Sub delete_zeros()
    Dim i, j As Long
    Dim ws As Worksheet
    
    i = 3   '�������� �������� � ������� ��������� (������ 3)
    j = 4   '��������� ������ � ������ ����� ������ 4
    
    '����� ����� ��� ��� ��������� ��� ���������� �������� ������� ��������
    '� ��������� ��� ������ �������� ������� ��������� ��������� ���������
    
    For Each ws In ActiveWorkbook.Worksheets
        If ws.name = "arr_SubAssemblies" Or ws.name = "arr_Parts" _
            Or ws.name = "arr_StandartParts" Or ws.name = "arr_Materials" Or ws.name = "arr_Others" Then
            
            j = 4
            Do While ws.Cells(j, 1) <> ""
                If ws.Cells(j, 6) = 0 Then
                ws.Rows(j).Delete
                    Select Case ws.name
                        Case "arr_SubAssemblies":   num_SubAssemblies = num_SubAssemblies - 1
                        Case "arr_Parts":           num_Parts = num_Parts - 1
                        Case "arr_StandartParts":   num_StandartParts = num_StandartParts - 1
                        Case "arr_Materials":       num_Materials = num_Materials - 1
                        Case "arr_Others":          num_Others = num_Others - 1
                        End Select
                Else: j = j + 1
                End If
            Loop
            
       End If
    Next
End Sub
