Attribute VB_Name = "addItemForm_getDataModule"
'� ���� ������ ��������� ��������� ����� ���������� �� �����
Option Explicit

Public Sub write_element(ByRef sheet As Worksheet, ByRef num As Long)
    Dim index, is_in_index As Long
    Dim i As Long
    
    num = num + 1
    index = num + 3
    
    With sheet
        .Cells(index, 1) = CStr(Date & " " & Time())
        .Cells(index, 2) = CStr(RTrim(mainModule.obj_addItemForm.textBox_Code))
        .Cells(index, 3) = CStr(mainModule.obj_addItemForm.textBox_Name)
        .Cells(index, 4) = CStr(mainModule.obj_addItemForm.textBox_In)
        .Cells(index, 5) = CDbl(mainModule.obj_addItemForm.textBox_Amount)
        .Cells(index, 8) = CStr(RTrim(mainModule.obj_addItemForm.textBox_GOST))
        .Cells(index, 21) = CStr(mainModule.obj_addItemForm.cb_Units)
        .Cells(index, 23) = 0       ' ��� ������ ����� ������ ����� ���� - ��������� � ��������� �� ���������
        .Cells(index, 24) = CLng(mainModule.obj_addItemForm.checkBox_toBuy) * (-1)  '*(-1) ������ ��� ���� ������� ������, �� ��� ����������� � long ������� �������� -1
        .Cells(index, 25) = CLng(mainModule.obj_addItemForm.checkBox_toCoop) * (-1)
        .Cells(index, 28) = CLng(mainModule.obj_addItemForm.checkBox_Painted) * (-1)
        
            If mainModule.obj_addItemForm.checkBox_noDwg.value = True Then
                .Cells(index, 26) = 1
                write_propInfo mainModule.obj_noDwgSideForm, index
            Else: .Cells(index, 26) = 0
            End If
        
        For i = 4 To num_RootAssemblies + 3
            If ActiveWorkbook.Worksheets("arr_RootAssemblies").Cells(i, 2) = mainModule.obj_addItemForm.textBox_In Then
                If ActiveWorkbook.Worksheets("arr_RootAssemblies").Cells(i, 28) = 1 Then .Cells(index, 28) = 1
            End If
        Next i
        
        For i = 4 To num_SubAssemblies + 3
            If ActiveWorkbook.Worksheets("arr_SubAssemblies").Cells(i, 2) = mainModule.obj_addItemForm.textBox_In Then
                If ActiveWorkbook.Worksheets("arr_SubAssemblies").Cells(i, 28) = 1 Then .Cells(index, 28) = 1
            End If
        Next i
        
        If .Cells(index, 28) = 1 And .Cells(index, 26) = 1 Then .Cells(index, 23) = 0 ' ���� ������ ������ �� � ���������, ������ ������ 0
        
    End With
End Sub

Public Sub delete_paint(ByVal paint, prefix, is_in As String)
    Dim i As Long
    
    For i = 4 To num_of_strings(ActiveWorkbook.Worksheets("arr_Materials")) + 3
        With ActiveWorkbook.Worksheets("arr_Materials")
        If .Cells(i, 2) = paint And .Cells(i, 4) = is_in And .Cells(i, 28) = prefix Then
            .Rows(i).Delete
            num_Materials = num_Materials - 1
        End If
        End With
    Next i
End Sub

Public Sub write_element_paint(ByRef sheet As Worksheet, ByRef num As Long)
    Dim index As Long
    Dim weight As Double '��� ���
    
    'num = num + 1
    index = num + 4
    
    With sheet
        .Cells(index, 1) = CStr(Date & " " & Time())
        .Cells(index, 2) = RTrim(mainModule.obj_propInputForm.cb_paint)
        .Cells(index, 4) = mainModule.obj_propInputForm.textBox_Code
        
        weight = CDbl(mainModule.obj_propInputForm.textBox_Area) * CDbl(mainModule.obj_propInputForm.textBox_Cons) * _
                CDbl(mainModule.obj_propInputForm.textBox_ConsCoeff) * CDbl(mainModule.obj_propInputForm.textBox_layersNum) / 1000000
        .Cells(index, 5) = weight
        .Cells(index, 8) = RTrim(mainModule.obj_propInputForm.cb_paintGOST)
        .Cells(index, 15) = mainModule.obj_propInputForm.textBox_Area
        .Cells(index, 16) = mainModule.obj_propInputForm.textBox_Cons
        .Cells(index, 17) = mainModule.obj_propInputForm.textBox_ConsCoeff
        .Cells(index, 18) = mainModule.obj_propInputForm.textBox_layersNum
        .Cells(index, 21) = "��"
    End With
    num_Materials = num_Materials + 1
    
End Sub

Public Sub write_element_paint_complex(ByRef sheet As Worksheet, ByRef num As Long, _
        ByVal paint, paintGOST, code, area, cons, conscoeff, layers_num, prefix As String)
    Dim index As Long
    Dim weight As Double '��� ���
    
    'num = num + 1
    index = num + 4
    
    With sheet
        .Cells(index, 1) = CStr(Date & " " & Time())
        .Cells(index, 2) = RTrim(paint)
        .Cells(index, 4) = code
        
        weight = CDbl(area) * CDbl(cons) * CDbl(conscoeff) * CDbl(layers_num) / 1000000
        .Cells(index, 5) = weight
        .Cells(index, 8) = RTrim(paintGOST)
        .Cells(index, 15) = area
        .Cells(index, 16) = cons
        .Cells(index, 17) = conscoeff
        .Cells(index, 18) = layers_num
        .Cells(index, 21) = "��"
        .Cells(index, 28) = prefix
    End With
    num_Materials = num_Materials + 1
    
End Sub

Public Sub set_element_painted(ByRef sheet As Worksheet, ByVal code As String, ByVal painted_flag As Long)
    '����������� �������� ����������� ��������� � ������������ string
    Dim i As Long
    For i = 4 To num_of_strings(sheet) + 3
        If sheet.Cells(i, 2) = code Then sheet.Cells(i, 28) = painted_flag
    Next i
End Sub

Public Sub check_format_num(ByRef KeyAscii As MSForms.ReturnInteger, _
                            ByRef text_box As MSForms.ComboBox) '�������� ����� � ���� ����������� � ������ �: ���.���.���[-XXX]
    '��� ����� ���������� ��������
    Select Case KeyAscii
        Case 8: 'backspace - ������ �� ������
        Case 32: text_box.Text = text_box.Text & "."    '������ ������� �� �����
                 KeyAscii = 0
        Case 48 To 57: ' 0..9
        Case 1040 To 1071, 1025:    '1040-1071 - "�..�", 1025 - "�"
        Case 1072 To 1103, 1105:    KeyAscii = AscW(UCase(ChrW(KeyAscii))) '��������� ��� � ������, ������������� ������� ������� � ���� ��� ������ ������� ���
                                    '1072 - 1103 - "�..�", 1105 - "�"
        Case 45: ' If count_symbols(text_box.Text, "-") > 0 Then KeyAscii = 0 ' 45 - ����� ASCII
        Case 46: ' If count_symbols(text_box.text, ".") > 1 Then KeyAscii = 0 ' 46 - ����� ASCII
        Case Else: KeyAscii = 0
    End Select
End Sub

Public Sub check_format_num_edit(ByRef KeyAscii As MSForms.ReturnInteger, _
                            ByRef text_box As MSForms.TextBox) '�������� ����� � ���� ����������� � ������ �: ���.���.���[-XXX]
    '��� ����� �������� ���������
    Select Case KeyAscii
        Case 8: 'backspace - ������ �� ������
        Case 32: text_box.Text = text_box.Text & "."    '������ ������� �� �����
                 KeyAscii = 0
        Case 48 To 57: ' 0..9
        Case 1040 To 1071, 1025:    '1040-1071 - "�..�", 1025 - "�"
        Case 1072 To 1103, 1105:    KeyAscii = AscW(UCase(ChrW(KeyAscii))) '��������� ��� � ������, ������������� ������� ������� � ���� ��� ������ ������� ���
                                    '1072 - 1103 - "�..�", 1105 - "�"
        Case 45: ' If count_symbols(text_box.Text, "-") > 0 Then KeyAscii = 0 ' 45 - ����� ASCII
        Case 46: ' If count_symbols(text_box.text, ".") > 1 Then KeyAscii = 0 ' 46 - ����� ASCII
        Case Else: KeyAscii = 0
    End Select
End Sub

Public Sub check_format_amount(ByRef KeyAscii As MSForms.ReturnInteger, _
                            ByRef text_box As MSForms.TextBox)  '�������� ����� � ���� ����������
        Select Case KeyAscii
        Case 8: 'backspace - ������ �� ������
        Case 32, 46: If count_symbols(text_box.Text, ",") > 0 Then KeyAscii = 0
                 If count_symbols(text_box.Text, ",") = 0 Then
                 text_box.Text = text_box.Text & ","    '������ ������� � ����� �� �������
                 KeyAscii = 0
                 End If
        Case 48 To 57: ' 0..9
        Case 44: If count_symbols(text_box.Text, ",") > 0 Then KeyAscii = 0 ' 44 - ������� ASCII
        Case Else: KeyAscii = 0
    End Select
End Sub

Public Function count_symbols(ByVal str As String, ByVal sym As String) As Long '��������� ������� � ������
    count_symbols = Len(str) - Len(Replace(str, sym, ""))
End Function

Public Function num_of_strings(ByRef sheet As Worksheet) As Long
    Dim i, num As Long
    i = 4
    num = 0
    
    Do While sheet.Cells(i, 1) <> ""
        num = num + 1
        i = i + 1
    Loop
    
    num_of_strings = num
End Function


