Attribute VB_Name = "propInputForm_errorsModule"
Option Explicit

Public Function if_propInfo_filled() As Boolean
    Dim bar_flag, paint_flag As Boolean
    
    With mainModule.obj_propInputForm
        If .cb_userbar <> "" And .cb_userbarGOST <> "" And .cb_usermaterial <> "" And .cb_usermaterialGOST <> "" And .textBox_ExactWeight <> "" _
            And .cb_bar.ListIndex <> -1 And .cb_density.ListIndex <> -1 Then
            If .cb_bar.ListIndex = 0 Or .cb_bar.ListIndex = 2 Or .cb_bar.ListIndex = 6 Then
                If .textBox_A <> "" And .textBox_B <> "" Then
                    bar_flag = True
                Else: if_propInfo_filled = False
                End If
            ElseIf .cb_bar.ListIndex = 1 Or .cb_bar.ListIndex = 4 Then
                If .textBox_A <> "" And .textBox_B <> "" And .textBox_C <> "" Then
                    bar_flag = True
                Else: if_propInfo_filled = False
                End If
            ElseIf .cb_bar.ListIndex = 3 Or .cb_bar.ListIndex = 5 Then
                If .textBox_A <> "" And .textBox_B <> "" And .textBox_C <> "" And .textBox_D <> "" Then
                    bar_flag = True
                Else: bar_flag = False
                End If
            End If
        Else
            bar_flag = False
        End If
        
        If .cb_TT.value = True Then
            If .textBox_layersNum.Enabled = True Then
                If .cb_paint <> "" And .cb_paintGOST <> "" And .textBox_Cons <> "" And .textBox_ConsCoeff <> "" And .textBox_Area <> "" Then
                    paint_flag = True
                Else: paint_flag = False
                End If
            ElseIf .textBox_layersNum.Enabled = False Then
                If .textBox_Area <> "" Then
                    paint_flag = True
                Else: paint_flag = False
                End If
            End If
            Else: paint_flag = True
        End If
        
        if_propInfo_filled = bar_flag And paint_flag
        
    End With
End Function
