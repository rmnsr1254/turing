Attribute VB_Name = "mainModule"
' � ���� ������ ��������� ���������, ���������� �� ���������� ������� ���������
' List - �������, sheets - ����� � �������

Option Explicit

Public obj_startForm As New startForm           ' ������ ��������� �����
Public obj_addItemForm As New addItemForm       ' ������ ����� ���������� �������
Public obj_editDeleteForm As New EditDeleteForm ' ������ ����� �������� ���������
Public obj_propInputForm As New propInputForm   ' ������ ����� ����� ���������� � ����������
Public obj_noDwgSideForm As New noDwgSideForm   ' ������ �������� ��� ����� ���������� � ������� ��� �������
Public obj_densityUpdateForm As New densityUpdateForm   '������ ����� ���������� ��������� � ������� ������

Public newListAdress As Variant                 ' ����� �������
Public numOfSheets As Long                      ' ���-�� ������ � �������
Public expanded_strings_num As Long             ' ���������� ����������� �����
Public currentRow As Long                       ' ���������� ���������� ������� ������ ��� ������ � �������

Public rootAssemblies_maxRow As Long            ' ����� ������, �� ������� ������������� ����� �������� ������
                                                ' � ���������� ����� ���������
                                                ' ����� - ����������
                                                
Public Const stepIn As Long = 4                 '��� ���������� �� �����
Public Const stepOut As Long = 3                '��� ������ �� �������
                                                
Public subAssemblies_maxRow As Long
Public parts_maxRow As Long
Public standartParts_maxRow As Long
Public materials_maxRow As Long
Public others_maxRow As Long

Public num_RootAssemblies As Long
Public num_SubAssemblies As Long
Public num_Parts As Long
Public num_StandartParts As Long
Public num_Materials As Long
Public num_Others As Long

Public logs_data() As Variant                    ' ������������ ������, � ������� ������������ ������ ����� ����� ��� ��� ���� ���������� �� ����

Public Sub on_Open()                             '��������� �������� ���������
    ThisWorkbook.Windows(1).Visible = False          '�������� �� FALSE ��� ������
    obj_startForm.Show vbModal
End Sub

Public Sub create_NewList()                     '�������� ������ �������
    obj_startForm.Hide
    newListAdress = Application.GetSaveAsFilename(Title:="�������� ����� ��� �������� �������", _
                                                    FileFilter:="Excel Files (*.xls), *.xls")
    If newListAdress = False Then
        MsgBox "������� �� ��� ������!"
        on_Open
    Else:
        ThisWorkbook.Worksheets(Array("blank_sheet", "arr_RootAssemblies", "arr_SubAssemblies", _
                                "arr_Parts", "arr_StandartParts", "arr_Materials", "arr_Others", "db_density", "integrated_materials", "report", "logs")).Copy
        ActiveWorkbook.SaveAs newListAdress
        ActiveWorkbook.Worksheets("blank_sheet").name = "�������"
        numOfSheets = 1
        expanded_strings_num = 0
        
        With ActiveWorkbook            '������ ����� ��� ������
            .Worksheets("arr_RootAssemblies").Visible = False
            .Worksheets("arr_SubAssemblies").Visible = False
            .Worksheets("arr_Parts").Visible = False
            .Worksheets("arr_StandartParts").Visible = False
            .Worksheets("arr_Materials").Visible = False
            .Worksheets("arr_Others").Visible = False
            .Worksheets("db_density").Visible = False
            .Worksheets("integrated_materials").Visible = False
            .Worksheets("report").Visible = False
            .Worksheets("logs").Visible = False
        End With
        
        page_setup
        
        num_RootAssemblies = 0
        num_SubAssemblies = 0
        num_Parts = 0
        num_StandartParts = 0
        num_Materials = 0
        num_Others = 0
                        
        MsgBox "������� ������� ������! ������� ��, ����� ������ ����������"
        
        obj_addItemForm.Show vbModeless
    
    logs_data = Array("������ ����� �������", newListAdress)
    drop_logs logs_data
    
    End If
End Sub

Public Sub continue_working()
    obj_startForm.Hide
    newListAdress = Application.GetOpenFilename(Title:="�������� ������� ��� ����������� ������: ", _
                                                    FileFilter:="Excel Files (*.xls), *.xls")
    If newListAdress = False Then
        MsgBox "������� �� ��� ������!"
        on_Open
    Else:
        numOfSheets = 1
        expanded_strings_num = 0
        
        Workbooks.Open newListAdress
        
        check_logs_presence
        
        With ActiveWorkbook            '������ ����� ��� ������
            .Worksheets("arr_RootAssemblies").Visible = False
            .Worksheets("arr_SubAssemblies").Visible = False
            .Worksheets("arr_Parts").Visible = False
            .Worksheets("arr_StandartParts").Visible = False
            .Worksheets("arr_Materials").Visible = False
            .Worksheets("arr_Others").Visible = False
            .Worksheets("db_density").Visible = False
            .Worksheets("integrated_materials").Visible = False
            .Worksheets("report").Visible = False
            .Worksheets("logs").Visible = False
        End With
        
        page_setup
        
        num_RootAssemblies = num_of_strings(ActiveWorkbook.Worksheets("arr_RootAssemblies"))
        num_SubAssemblies = num_of_strings(ActiveWorkbook.Worksheets("arr_SubAssemblies"))
        num_Parts = num_of_strings(ActiveWorkbook.Worksheets("arr_Parts"))
        num_StandartParts = num_of_strings(ActiveWorkbook.Worksheets("arr_StandartParts"))
        num_Materials = num_of_strings(ActiveWorkbook.Worksheets("arr_Materials"))
        num_Others = num_of_strings(ActiveWorkbook.Worksheets("arr_Others"))
             
        MsgBox "������� ������� ������! ������� ��, ����� ���������� ������"
        obj_addItemForm.Show vbModeless
        
        logs_data = Array("������ �������", newListAdress)
        drop_logs logs_data
        
    End If
End Sub

Public Sub check_logs_presence()
Dim flag As Boolean
Dim ws As Worksheet

flag = False

    For Each ws In ActiveWorkbook.Worksheets
        If ws.name = "logs" Then
            flag = True
            Exit For
        End If
    Next
    
    If flag = False Then ActiveWorkbook.Sheets.Add.name = "logs"
End Sub


