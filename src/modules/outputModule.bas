Attribute VB_Name = "outputModule"
'� ���� ������ ��������� ��������� ������ �������� � ���������� � ������� Excel

Option Explicit

Public Sub add_sheet(ByRef numOfSheets As Long)      '�������� ���� � �������
Dim insert_row As Long
insert_row = numOfSheets * 39 + 1 - expanded_strings_num

    ThisWorkbook.Worksheets("blank_sheet").Range("A1:M39").Copy _
        Destination:=ActiveWorkbook.Worksheets("�������").Range("A" & insert_row & ":M" & insert_row + 39) '����������� ���������
        
    ActiveWorkbook.Worksheets("�������").Range("A" & insert_row & ":M" & insert_row).RowHeight = _
                                ThisWorkbook.Worksheets("blank_sheet").Range("A1").EntireRow.Height    '����������� ������ ������-���������
                
    ActiveWorkbook.Worksheets("�������").Range("A" & insert_row + 1 & ":M" & insert_row + 39 - 1).RowHeight = _
                                ThisWorkbook.Worksheets("blank_sheet").Range("A2").EntireRow.Height   '����������� ����� ������ ���������
            
    numOfSheets = numOfSheets + 1
End Sub

Public Sub excel_list_update(ByRef sheet As Worksheet, rowNumber As Long)    '����� �� ���� ������ ������ ��� ���������
            
Dim i As Long
i = 4

            Do While sheet.Cells(i, 1) <> ""
                If i > 4 Then
                    If sheet.Cells(i - 1, 2) = sheet.Cells(i, 2) Then
                    rowNumber = rowNumber + 1
                    Else: rowNumber = rowNumber + stepOut
                    End If
                Else: rowNumber = rowNumber + stepOut
                End If
                
                check_for_new_sheet rowNumber
                
                With ActiveWorkbook.Worksheets("�������")
                    If sheet.Cells(i, 2) <> "" Then .Cells(rowNumber, 1) = sheet.Cells(i, 2)
                    If sheet.Cells(i, 3) <> "" Then .Cells(rowNumber, 2) = sheet.Cells(i, 3)
                    If sheet.Cells(i, 4) <> "" Then .Cells(rowNumber, 3) = sheet.Cells(i, 4)
                    If sheet.Cells(i, 5) <> "" Then .Cells(rowNumber, 4) = sheet.Cells(i, 5)
                    If sheet.Cells(i, 8) <> "" Then .Cells(rowNumber, 13) = sheet.Cells(i, 8)
                    If CLng(sheet.Cells(i, 24)) = 1 Then       '���� ������ �� ������ � �������
                            
                                .Cells(rowNumber, 1) = sheet.Cells(i, 2)
                                .Cells(rowNumber, 1).Font.ColorIndex = 16
                                
                                .Cells(rowNumber, 2) = sheet.Cells(i, 3)
                                .Cells(rowNumber, 2).Font.ColorIndex = 16
                                
                                .Cells(rowNumber, 3) = sheet.Cells(i, 4)
                                .Cells(rowNumber, 3).Font.ColorIndex = 16
                                
                                .Cells(rowNumber, 4) = sheet.Cells(i, 5)
                                .Cells(rowNumber, 4).Font.ColorIndex = 16
                                
                                .Cells(rowNumber, 5) = sheet.Cells(i, 6)
                                .Cells(rowNumber, 5).Font.ColorIndex = 16
                                
                                .Cells(rowNumber, 7) = "���������� ��� ������� ���������"
                    End If
                End With
            i = i + 1
            Loop

            add_sheet numOfSheets
            rowNumber = (numOfSheets - 1) * 39 + 2
            
End Sub

Public Sub excel_output(ByRef sheet As Worksheet, ByRef rowNumber As Long)     '������ � ������-�������
    
Dim i, buffer As Long
Dim coop_marker As String   '   ��������� ���� �������� "", ���� "(�) " - ��� ��������� � �����������
i = 4
            Do While sheet.Cells(i, 1) <> ""
                If i > 4 Then
                '����������, ����� �� ���������� �� ���� ������� ��� �� ����� ���
                    If sheet.Cells(i - 1, 2) = sheet.Cells(i, 2) And sheet.Cells(i - 1, 8) = sheet.Cells(i, 8) Then
                    rowNumber = rowNumber + 1
                    Else: rowNumber = rowNumber + stepOut
                    End If
                Else: rowNumber = rowNumber + stepOut
                End If
                
                check_for_new_sheet rowNumber
                
                With ActiveWorkbook.Worksheets("�������")
                    If sheet.Cells(i, 2) <> "" Then
                        If CLng(sheet.Cells(i, 24)) = 0 Then    '���� ������� ������ � �������
                            .Cells(rowNumber, 1) = sheet.Cells(i, 2)
                            .Cells(rowNumber, 13) = sheet.Cells(i, 8)
                            If sheet.Cells(i, 3) = "" Then
                                .Cells(rowNumber, 2) = Empty
                                If sheet.Cells(i - 1, 2) <> sheet.Cells(i, 2) Or sheet.Cells(i - 1, 8) <> sheet.Cells(i, 8) Then .Range(.Cells(rowNumber, 1), .Cells(rowNumber, 2)).Merge
                            Else:
                                .Cells(rowNumber, 2) = sheet.Cells(i, 3)
                            End If
                            
                            If sheet.Cells(i, 4) = "" Then
                                .Cells(rowNumber, 3) = Empty
                                If sheet.Cells(i - 1, 2) <> sheet.Cells(i, 2) Or sheet.Cells(i - 1, 8) <> sheet.Cells(i, 8) Then .Range(.Cells(rowNumber, 2), .Cells(rowNumber, 3)).Merge
                            Else:
                                .Cells(rowNumber, 3) = sheet.Cells(i, 4)
                            End If
                            
                            .Cells(rowNumber, 4) = CStr(Round(sheet.Cells(i, 5), 3))
                                
                                If sheet.Cells(i, 6) > 0 Then
                                    .Cells(rowNumber, 5) = CStr(Round(sheet.Cells(i, 6), 3))
                                        If sheet.Cells(i, 7) > 0 Then
                                            If i > 0 Then
                                                If sheet.Cells(i - 1, 2) = sheet.Cells(i, 2) And sheet.Cells(i - 1, 8) = sheet.Cells(i, 8) Then
                                                        .Cells(rowNumber, 1).Font.Color = vbWhite
                                                        .Cells(rowNumber, 2).Font.Color = vbWhite
                                                        .Cells(rowNumber, 6) = Empty
                                                        .Cells(rowNumber, 7) = Empty
                                                        .Cells(rowNumber, 8) = Empty
                                                        .Cells(rowNumber, 9) = Empty
                                                        .Cells(rowNumber, 10) = Empty
                                                        .Cells(rowNumber, 11) = Empty
                                                        .Cells(rowNumber, 12) = Empty
                                                        .Cells(rowNumber, 13).Font.Color = vbWhite
                                                    Else:
                                                    
                                                        .Cells(rowNumber, 6) = CStr(Round(sheet.Cells(i, 7), 2))
                                                        .Cells(rowNumber, 7) = CStr(sheet.Cells(i, 9))
                                                        
                                                        If sheet.Cells(i, 10) <> "" And sheet.Cells(i, 12) <> "" Then
                                                            .Cells(rowNumber, 8) = CStr(sheet.Cells(i, 10))
                                                            .Cells(rowNumber, 9) = CStr(sheet.Cells(i, 12))
                                                        Else:
                                                            .Cells(rowNumber, 8) = Empty
                                                            .Cells(rowNumber, 9) = Empty
                                                        End If
                                                                                                                                                        
                                                        If sheet.Cells(i, 14) = "" Then
                                                            buffer = -1
                                                        Else: buffer = CLng(sheet.Cells(i, 14))
                                                        End If
                                                        Select Case buffer
                                                            Case -1: .Cells(rowNumber, 10) = Empty
                                                            Case 0: .Cells(rowNumber, 10) = "D" + CStr(sheet.Cells(i, 15)) + "x" + CStr(sheet.Cells(i, 16))
                                                            Case 1: .Cells(rowNumber, 10) = CStr(sheet.Cells(i, 15)) + "x" + CStr(sheet.Cells(i, 16)) + "x" + CStr(sheet.Cells(i, 17))
                                                            Case 2: .Cells(rowNumber, 10) = "H=" + CStr(sheet.Cells(i, 15)) + ", L=" + CStr(sheet.Cells(i, 16))
                                                            Case 3: .Cells(rowNumber, 10) = CStr(sheet.Cells(i, 15)) + "x" + CStr(sheet.Cells(i, 16)) + "x" + CStr(sheet.Cells(i, 17)) + " L=" + CStr(sheet.Cells(i, 18))
                                                            Case 4: .Cells(rowNumber, 10) = "D" + CStr(sheet.Cells(i, 15)) + " S = " + CStr(sheet.Cells(i, 16)) + " L = " + CStr(sheet.Cells(i, 17))
                                                            Case 5: .Cells(rowNumber, 10) = CStr(sheet.Cells(i, 15)) + "x" + CStr(sheet.Cells(i, 16)) + " S=" + CStr(sheet.Cells(i, 17)) + " L=" + CStr(sheet.Cells(i, 18))
                                                            Case 6: .Cells(rowNumber, 10) = "L = " + CStr(sheet.Cells(i, 16))
                                                        End Select
                                                        
                                                        .Cells(rowNumber, 11) = sheet.Cells(i, 21)
                                                        
                                                        If sheet.Cells(i, 22) <> "" Then
                                                            .Cells(rowNumber, 12) = WorksheetFunction.Round(CDbl(sheet.Cells(i, 22)), 2)
                                                            Else: .Cells(rowNumber, 12) = Empty
                                                        End If
                                                        
                                                        Select Case sheet.Cells(i, 25)
                                                            Case 1:
                                                                    If sheet.Cells(i, 27) = "" Then
                                                                      coop_marker = "(�) "
                                                                    Else: coop_marker = "(" & sheet.Cells(i, 27) & ") "
                                                                    End If
                                                            Case 0: coop_marker = ""
                                                        End Select
                                                                                                        
                                                        If sheet.Cells(i, 11) <> "" And sheet.Cells(i, 13) <> 0 Then
                                                            .Cells(rowNumber, 13) = coop_marker + sheet.Cells(i, 11) + " / " + sheet.Cells(i, 13)
                                                        Else
                                                             .Cells(rowNumber, 13) = sheet.Cells(i, 8)
                                                        End If
                                                                                                            
                                                End If
                                            Else:
                                                .Cells(rowNumber, 6) = CStr(Round(sheet.Cells(i, 7), 2))
                                                .Cells(rowNumber, 7) = CStr(sheet.Cells(i, 9))
                                                .Cells(rowNumber, 8) = CStr(sheet.Cells(i, 10)) + CStr(sheet.Cells(i, 11))
                                                .Cells(rowNumber, 9) = CStr(sheet.Cells(i, 12)) + CStr(sheet.Cells(i, 13))
                                                If sheet.Cells(i, 14) = "" Then
                                                            buffer = -1
                                                Else: buffer = CLng(sheet.Cells(i, 14))
                                                End If
                                                        Select Case buffer
                                                            Case -1: .Cells(rowNumber, 10) = ""
                                                            Case 0: .Cells(rowNumber, 10) = "D" + sheet.Cells(i, 15) + "x" + sheet.Cells(i, 16)
                                                            Case 1: .Cells(rowNumber, 10) = sheet.Cells(i, 15) + "x" + sheet.Cells(i, 16) + "x" + sheet.Cells(i, 17)
                                                            Case 2: .Cells(rowNumber, 10) = "H=" + sheet.Cells(i, 15) + ", L=" + sheet.Cells(i, 16)
                                                            Case 3: .Cells(rowNumber, 10) = sheet.Cells(i, 15) + "x" + sheet.Cells(i, 16) + "x" + sheet.Cells(i, 17) + " L = " + sheet.Cells(i, 18)
                                                            Case 4: .Cells(rowNumber, 10) = "D" + sheet.Cells(i, 15) + " S = " + sheet.Cells(i, 16) + " L=" + sheet.Cells(i, 17)
                                                            Case 5: .Cells(rowNumber, 10) = sheet.Cells(i, 15) + "x" + sheet.Cells(i, 16) + " S=" + sheet.Cells(i, 17) + " L=" + sheet.Cells(i, 18)
                                                        End Select
                                                        
                                                .Cells(rowNumber, 11) = CStr(sheet.Cells(i, 21))
                                                
                                                If sheet.Cells(i, 22) <> "" Then
                                                    .Cells(rowNumber, 12) = WorksheetFunction.Round(CDbl(sheet.Cells(i, 22)), 2)
                                                Else: .Cells(rowNumber, 12) = Empty
                                                End If
                                                
                                                Select Case sheet.Cells(i, 25)
                                                            Case 1:
                                                                    If sheet.Cells(i, 27) = "" Then
                                                                      coop_marker = "(�) "
                                                                    Else: coop_marker = "(" & sheet.Cells(i, 27) & ") "
                                                                    End If
                                                            Case 0: coop_marker = ""
                                                        End Select
                                                
                                                If sheet.Cells(i, 11) <> "" And sheet.Cells(i, 13) <> 0 Then
                                                    .Cells(rowNumber, 13) = coop_marker + sheet.Cells(i, 11) + " / " + sheet.Cells(i, 13)
                                                Else
                                                    .Cells(rowNumber, 13) = sheet.Cells(i, 8)
                                                End If
                                                
                                            End If
                                        End If
                                End If
                                
                                
                            ElseIf CLng(sheet.Cells(i, 24)) = 1 Then       '���� ������ �� ������ � �������
                                    
                                    .Cells(rowNumber, 1) = sheet.Cells(i, 2)
                                    .Cells(rowNumber, 1).Font.ColorIndex = 16
                                    
                                    .Cells(rowNumber, 2) = sheet.Cells(i, 3)
                                    .Cells(rowNumber, 2).Font.ColorIndex = 16
                                    
                                    .Cells(rowNumber, 3) = sheet.Cells(i, 4)
                                    .Cells(rowNumber, 3).Font.ColorIndex = 16
                                    
                                    .Cells(rowNumber, 4) = sheet.Cells(i, 5)
                                    .Cells(rowNumber, 4).Font.ColorIndex = 16
                                    
                                    .Cells(rowNumber, 5) = sheet.Cells(i, 6)
                                    .Cells(rowNumber, 5).Font.ColorIndex = 16
                                    
                                    .Cells(rowNumber, 6) = sheet.Cells(i, 7)
                                    .Cells(rowNumber, 6).Font.ColorIndex = 16
                                    
                                    .Cells(rowNumber, 7) = "���������� ��� ������� ���������"
                                    .Cells(rowNumber, 7).Font.ColorIndex = 16
                                    
                                    .Cells(rowNumber, 13) = sheet.Cells(i, 8)
                                    .Cells(rowNumber, 13).Font.ColorIndex = 16
                                    
                                If sheet.Cells(i - 1, 2) = sheet.Cells(i, 2) Then
                                
                                    .Cells(rowNumber, 1) = sheet.Cells(i, 2)
                                    .Cells(rowNumber, 1).Font.Color = vbWhite
                                    
                                    .Cells(rowNumber, 2) = sheet.Cells(i, 3)
                                    .Cells(rowNumber, 2).Font.Color = vbWhite
                                    
                                    .Cells(rowNumber, 6) = sheet.Cells(i, 7)
                                    .Cells(rowNumber, 6).Font.Color = vbWhite
                                    
                                    .Cells(rowNumber, 7) = Empty
                                End If
                                
                            End If
                            
                           '     If Len(.Cells(rowNumber, 1)) > 49 Or _
                            '        Len(.Cells(rowNumber, 2)) > 22 Or _
                             '       Len(.Cells(rowNumber, 13)) > 35 Then
                            If sheet.Cells(i - 1, 2) <> sheet.Cells(i, 2) Or sheet.Cells(i - 1, 8) <> sheet.Cells(i, 8) Then
                                    Select Case CStr(sheet.name)
                                        Case "arr_RootAssemblies": format_code rowNumber, 2, 50
                                        Case "arr_SubAssemblies", "arr_Parts": format_code rowNumber, 2, 20
                                        Case "arr_StandartParts", "arr_Materials", "arr_Others": format_code rowNumber, 1, 50
                                                                                                    format_code rowNumber, 13, 50
                                    End Select
                            End If
                              '  End If
                            
                    End If
                End With
            i = i + 1
            Loop
            
            add_sheet numOfSheets
            rowNumber = (numOfSheets - 1) * 39 + 2 - expanded_strings_num
End Sub

Public Sub check_for_new_sheet(ByRef rowNumber As Long)
    If rowNumber > 39 * numOfSheets - 2 - expanded_strings_num Then
        add_sheet numOfSheets
        rowNumber = (numOfSheets - 1) * 39 - expanded_strings_num + 2
    End If
End Sub

Public Sub clean_excel_list_OLD(ByRef ListToClean As Worksheet, ByRef numOfSheets As Long)

    Dim i As Long
    
    For i = 0 To numOfSheets - 1                '������� � ������������ �����
    ListToClean.Range("A" & (2 + i * 39) & ":M" & (37 + i * 39)).UnMerge
    ThisWorkbook.Worksheets("blank_sheet").Range("A2:M37").Copy ListToClean.Range("A" & (2 + i * 39) & ":M" & (37 + i * 39))
    
    Next i
    
    numOfSheets = 1
End Sub

Public Sub clean_excel_list(ByRef ListToClean As Worksheet, ByRef numOfSheets As Long)
     
    ListToClean.Range("A2:M39").UnMerge
    ListToClean.Range("A2:M39").RowHeight = ThisWorkbook.Worksheets("blank_sheet").Cells(2, 1).RowHeight
    ThisWorkbook.Worksheets("blank_sheet").Range("A2:M39").Copy ListToClean.Range("A2:M39")
    
    ListToClean.Range(Cells(40, 1), Cells(numOfSheets * 40, 13)).Delete
    
    numOfSheets = 1
End Sub

Public Sub enumerate()   '����������� ��������� ������
    
    Dim i, sheet_counter As Long
    
    i = 1
    sheet_counter = 1
    Do While sheet_counter <= numOfSheets And i < 10000
        With ActiveWorkbook.Worksheets("�������")
            Select Case .Cells(i, 11)
               Case "����": .Cells(i, 13) = sheet_counter
                            sheet_counter = sheet_counter + 1
               Case "������": .Cells(i, 13) = numOfSheets
               
            End Select
            i = i + 1
        End With
    Loop
    
End Sub

Public Sub create_report()
    ActiveWorkbook.Worksheets("integrated_materials").Visible = True
End Sub

Public Sub format_code(ByRef row, column, base_length As Long)
    '����� �������� ����������� (��������, � ������ ������ �������)
Dim expand_coeff, i, buffer_row As Long

With ActiveWorkbook.Worksheets("�������")
    
    If Len(.Cells(row, column)) >= base_length Then
        If ThisWorkbook.Worksheets("blank_sheet").Cells(2, 1).RowHeight * WorksheetFunction.RoundUp(Len(.Cells(row, column)) / base_length, 0) >= .Cells(row, column).RowHeight Then
            expand_coeff = WorksheetFunction.RoundUp(Len(.Cells(row, column)) / base_length, 0)
            If row + expand_coeff - 1 >= 39 * numOfSheets - 2 - expanded_strings_num Then
                add_sheet numOfSheets
                buffer_row = row
                ActiveWorkbook.Worksheets("�������").Range(Cells(row, 1), Cells(row, 13)).Copy
                row = (numOfSheets - 1) * 39 - expanded_strings_num + 2
                ActiveWorkbook.Worksheets("�������").Range(Cells(row, 1), Cells(row, 13)).Insert
                ActiveWorkbook.Worksheets("�������").Range(Cells(buffer_row, 1), Cells(buffer_row, 13)).ClearContents
                ActiveWorkbook.Worksheets("�������").Range(Cells(buffer_row, 1), Cells(buffer_row, 13)).UnMerge
            End If
            
            .Cells(row, column).WrapText = True
            .Cells(row, column).Font.Size = 9
            
            .Cells(row, column).RowHeight = ThisWorkbook.Worksheets("blank_sheet").Cells(2, 1).RowHeight * expand_coeff
            
            For i = 1 To expand_coeff - 1
                .Rows(row + i).Delete
            Next i
            expanded_strings_num = expanded_strings_num + expand_coeff - 1
        End If
    End If
End With

End Sub

Public Sub drop_logs(ByRef logs_data() As Variant)
' ������ �����:     �����   ���_�������     ���_�������     ��������      �������_���������_1    �������_���������_2
Dim last_row, col_num As Long
Dim attrib As Variant

col_num = 1

    With ActiveWorkbook.Worksheets("logs")
       last_row = .Cells(1, 1).CurrentRegion.Rows.count + 1
       .Cells(last_row, col_num) = Date & " " & Time()
       col_num = col_num + 1
            For Each attrib In logs_data
             .Cells(last_row, col_num) = attrib
             col_num = col_num + 1
            Next
    End With
End Sub

Public Sub page_setup()
    '��������� ����������� �����
    ActiveWindow.View = xlPageLayoutView
    ActiveWindow.Zoom = 70
    Application.PrintCommunication = False
        ActiveSheet.PageSetup.Orientation = xlLandscape
        ActiveSheet.PageSetup.PaperSize = xlPaperA3
   ' Application.PrintCommunication = True
End Sub













