Attribute VB_Name = "propInputForm_ctrlInteraction"
Option Explicit

Public selected_part_index As Long
Public selected_paint_index As Long

Public Sub listbox_update(ByVal setFocus_index As Long)
' ���������� ��������� �� ������� ������� � ����������
Dim i, count As Long
Dim status As String
    
    With mainModule.obj_propInputForm.ListBox1
        .Clear
        
        i = 4
        count = 0
        
        For i = 4 To 4 + num_Parts - 1
            If ActiveWorkbook.Worksheets("arr_Parts").Cells(i, 2) <> ActiveWorkbook.Worksheets("arr_Parts").Cells(i - 1, 2) _
                    And ActiveWorkbook.Worksheets("arr_Parts").Cells(i, 24) = 0 Then
                Select Case CBool(ActiveWorkbook.Worksheets("arr_Parts").Cells(i, 23))
                    Case 1: status = "OK"
                    Case 0: status = "..."
                End Select
                .AddItem (CStr(count + 1) + "." + vbTab + status + vbTab + _
                CStr(ActiveWorkbook.Worksheets("arr_Parts").Cells(i, 2)) + vbTab + vbTab + "-" + vbTab + CStr(ActiveWorkbook.Worksheets("arr_Parts").Cells(i, 3)))
            count = count + 1
            End If
        Next i
    
    If num_Parts > 0 Then .ListIndex = setFocus_index
    
    End With
End Sub


Public Function check_if_complex(ByVal paint As String) As Boolean
    Dim i As Long
        i = 4
        Do While ThisWorkbook.Worksheets("db_paint").Cells(i, 1) <> ""
           If ThisWorkbook.Worksheets("db_paint").Cells(i, 2) = paint Then
            check_if_complex = True
            Exit Do
           End If
           i = i + 1
        Loop
End Function

Public Sub lable_update()
' ���������� ����� ������������ ������� � ���������� ����������� ������ �������

Dim i, count_filled, count_all As Long

    count_filled = 0
    count_all = 0
    
    For i = 4 To num_Parts - 1 + 4
        If ActiveWorkbook.Worksheets("arr_Parts").Cells(i, 2) <> ActiveWorkbook.Worksheets("arr_Parts").Cells(i - 1, 2) And _
             ActiveWorkbook.Worksheets("arr_Parts").Cells(i, 24) = 0 Then count_all = count_all + 1
        If ActiveWorkbook.Worksheets("arr_Parts").Cells(i, 23) = 1 And _
                ActiveWorkbook.Worksheets("arr_Parts").Cells(i, 2) <> ActiveWorkbook.Worksheets("arr_Parts").Cells(i - 1, 2) Then count_filled = count_filled + 1
    Next i

    With mainModule.obj_propInputForm
    .lb_amount.Caption = CStr(count_filled) + " / " + CStr(count_all)
    
    Select Case count_filled = count_all
        Case True: .cb_bill.Enabled = True
        Case False: .cb_bill.Enabled = False
    End Select
    End With
End Sub

Public Sub display_item(ByVal code_mashed As String)

    Dim code As String
    selected_part_index = 0
        
    code = Trim(Split(code_mashed, vbTab)(2))
    selected_part_index = find_part_by_code(code)
       
    With mainModule.obj_propInputForm
        .textBox_Code = ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 2)
        .textBox_Name = ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 3)
        .textBox_SumAmount = ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 7)
        
        .textBox_ExactWeight = ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 9)
        .cb_usermaterial = ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 10)
        .cb_usermaterialGOST = ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 11)
        .cb_userbar = ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 12)
        .cb_userbarGOST = ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 13)
        
        If ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 14) <> "" Then
            .cb_bar.ListIndex = CLng(ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 14))
        Else: .cb_bar.ListIndex = -1
        End If
        
        .textBox_A = ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 15)
        .textBox_B = ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 16)
        .textBox_C = ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 17)
        .textBox_D = ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 18)
        
        If ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 20) <> "" Then
        .cb_density.ListIndex = CLng(ActiveWorkbook.Worksheets("arr_Parts").Cells(selected_part_index, 20))
        Else: .cb_density.ListIndex = -1
        End If
        
        .checkBox_toCoop.value = CBool(ActiveWorkbook.Worksheets("arr_parts").Cells(selected_part_index, 25))
        .textBox_coop_def = CStr(ActiveWorkbook.Worksheets("arr_parts").Cells(selected_part_index, 27))
        
        '_____________���__________________
        
        .cb_TT.value = CBool(ActiveWorkbook.Worksheets("arr_parts").Cells(selected_part_index, 28))
        
        If .cb_TT.value = True Then
            get_paint_list .textBox_Code
            mainModule.obj_propInputForm.cb_deletePaint.Enabled = False
            If .ListBox2.ListCount > 0 Then .cb_TT.Enabled = False
        Else
            .ListBox2.Clear
            .cb_paint = ""
            .cb_paintGOST = ""
            .textBox_Cons = ""
            .textBox_ConsCoeff = ""
            .textBox_Area = ""
            
                        .textBox_Area.Enabled = False
                        .cb_paint.Enabled = False
                        .cb_paintGOST.Enabled = False
                        .textBox_Cons.Enabled = False
                        .textBox_ConsCoeff.Enabled = False
                        .cb_setPaint.Enabled = False
                        .cb_deletePaint.Enabled = False
        End If
    End With
    
End Sub

Public Sub display_paint()
    Dim paint, prefix, is_in As String
    
    With mainModule.obj_propInputForm
    
    prefix = .ListBox2.List(.ListBox2.ListIndex, 0)
    paint = .ListBox2.List(.ListBox2.ListIndex, 1)
    is_in = .textBox_Code
    
    selected_paint_index = find_paint_index(paint, prefix, is_in)
    
    .cb_paint = ActiveWorkbook.Worksheets("arr_Materials").Cells(selected_paint_index, 2)
    .cb_paintGOST = ActiveWorkbook.Worksheets("arr_Materials").Cells(selected_paint_index, 8)
    
    .textBox_Area = ActiveWorkbook.Worksheets("arr_Materials").Cells(selected_paint_index, 15)
    .textBox_Cons = ActiveWorkbook.Worksheets("arr_Materials").Cells(selected_paint_index, 16)
    .textBox_ConsCoeff = ActiveWorkbook.Worksheets("arr_Materials").Cells(selected_paint_index, 17)
    .textBox_layersNum = ActiveWorkbook.Worksheets("arr_Materials").Cells(selected_paint_index, 18)
       
    .cb_deletePaint.Enabled = True
    End With
End Sub

Public Sub get_paint_list(ByVal reference_code As String)
    ' ���������� ��������� �� ������� �������� ��������� ������
Dim i, count As Long
    
    With mainModule.obj_propInputForm.ListBox2
        .Clear
        
        i = 4
        count = 0
        
        For i = 4 To 4 + num_Materials - 1
            If ActiveWorkbook.Worksheets("arr_Materials").Cells(i, 4) = reference_code Then
                .AddItem CStr(ActiveWorkbook.Worksheets("arr_Materials").Cells(i, 28))
                .List(count, 1) = CStr(ActiveWorkbook.Worksheets("arr_Materials").Cells(i, 2))
            count = count + 1
            End If
        Next i
    
    End With
    
End Sub

Public Sub display_recalled_item(ByVal code_mashed As String)

    Dim code As String
    Dim prev_part_index As Long
    
    prev_part_index = 0
        
    code = Trim(Split(code_mashed, vbTab)(2))
    prev_part_index = find_part_by_code(code)
       
    With mainModule.obj_propInputForm
        .cb_usermaterial = ActiveWorkbook.Worksheets("arr_Parts").Cells(prev_part_index, 10)
        .cb_usermaterialGOST = ActiveWorkbook.Worksheets("arr_Parts").Cells(prev_part_index, 11)
        .cb_userbar = ActiveWorkbook.Worksheets("arr_Parts").Cells(prev_part_index, 12)
        .cb_userbarGOST = ActiveWorkbook.Worksheets("arr_Parts").Cells(prev_part_index, 13)
        
        If ActiveWorkbook.Worksheets("arr_Parts").Cells(prev_part_index, 14) <> "" Then
            .cb_bar.ListIndex = CLng(ActiveWorkbook.Worksheets("arr_Parts").Cells(prev_part_index, 14))
        Else: .cb_bar.ListIndex = -1
        End If
        
        .textBox_A = ""
        .textBox_B = ""
        .textBox_C = ""
        .textBox_D = ""
        .textBox_ExactWeight = ""
        
        If ActiveWorkbook.Worksheets("arr_Parts").Cells(prev_part_index, 20) <> "" Then
        .cb_density.ListIndex = CLng(ActiveWorkbook.Worksheets("arr_Parts").Cells(prev_part_index, 20))
        Else: .cb_density.ListIndex = -1
        End If
        
    End With
End Sub

Public Function find_part_by_code(ByVal code As String) As Long
    Dim i As Long
    For i = 0 To num_Parts - 1
        If CStr(ActiveWorkbook.Worksheets("arr_Parts").Cells(i + 4, 2)) = CStr(code) Then
           Exit For
        End If
    Next i
     find_part_by_code = i + 4
End Function

Public Function find_paint_index(ByVal paint, prefix, is_in As String) As Long
    Dim i As Long
    
    For i = 0 To num_Materials - 1
        If CStr(ActiveWorkbook.Worksheets("arr_Materials").Cells(i + 4, 2)) = CStr(paint) And _
            CStr(ActiveWorkbook.Worksheets("arr_Materials").Cells(i + 4, 4)) = CStr(is_in) And _
            CStr(ActiveWorkbook.Worksheets("arr_Materials").Cells(i + 4, 28)) = CStr(prefix) Then
           Exit For
        End If
    Next i
     find_paint_index = i + 4
End Function

Public Sub write_propInfo(ByRef form As UserForm, ByVal index As Long)

'������ ������ �� ����� ����� form � ������ � ������� index
Dim V, M As Double
Dim i As Long

    With form
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 9) = CStr(.textBox_ExactWeight)
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 10) = CStr(.cb_usermaterial)
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 11) = CStr(.cb_usermaterialGOST)
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 12) = CStr(.cb_userbar)
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 13) = CStr(.cb_userbarGOST)
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 14) = CStr(.cb_bar.ListIndex)
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 20) = CStr(.cb_density.ListIndex)
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 15) = .textBox_A
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 16) = .textBox_B
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 17) = .textBox_C
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 18) = .textBox_D
        
        Select Case .cb_bar.ListIndex
            Case 0: V = CDbl(.textBox_B) * 3.14 * 0.25 * (CDbl(.textBox_A) ^ 2)   '����� ��������, ��
            Case 1: V = CDbl(.textBox_A) * CDbl(.textBox_B) * CDbl(.textBox_C)    '����� ������, ��
            Case 2: V = 0.8658 * CDbl(.textBox_A) ^ 2 * CDbl(.textBox_B)          '����� ������������ ������, ��
            Case 3: V = CDbl(.textBox_A) * CDbl(.textBox_D) * CDbl(.textBox_C) + _
                        CDbl(.textBox_B) * CDbl(.textBox_D) * CDbl(.textBox_C) - (CDbl(.textBox_C) ^ 2) * CDbl(.textBox_D) ' ����� ������
            Case 4: V = 0.25 * 3.14 * CDbl(.textBox_C) * _
                            (CDbl(.textBox_A) ^ 2 - (CDbl(.textBox_A) - 2 * CDbl(.textBox_B)) ^ 2) '����� ������� �����
            Case 5: V = CDbl(.textBox_A) * CDbl(.textBox_B) * CDbl(.textBox_D) - _
                        (CDbl(.textBox_A) - 2 * CDbl(.textBox_C)) * (CDbl(.textBox_B) - 2 * CDbl(.textBox_C)) * CDbl(.textBox_D) '����� ������������� �����
            Case 6: V = 1000 * CDbl(.textBox_A) * CDbl(.textBox_B) / 7.85        '����� ������ ����� ������� �� �������� ����� ������������ ��������� �������
        End Select
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 19) = CStr(Round(V, 2))
        
        M = CDbl(ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 19) * db_density(.cb_density.ListIndex).density) * 0.001 * 0.001  '� �����������
        
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 22) = M
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 21) = "��"
                
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 23) = 1
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 25) = CLng(form.checkBox_toCoop.value) * (-1)
        
        ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 27) = CStr(form.textBox_coop_def)
         End With
         
        If mainModule.obj_addItemForm.checkBox_noDwg = False Then
            
            ActiveWorkbook.Worksheets("arr_Parts").Cells(index, 28) = form.cb_TT.value
                    
       
        
        i = 4
    
        Do While ActiveWorkbook.Worksheets("arr_parts").Cells(i, 1) <> ""
            If ActiveWorkbook.Worksheets("arr_parts").Cells(i, 2) = ActiveWorkbook.Worksheets("arr_parts").Cells(index, 2) And i <> index Then
            With ActiveWorkbook.Worksheets("arr_parts")
            .Range(.Cells(index, 9), .Cells(index, 28)).Copy Destination:=.Range(.Cells(i, 9), .Cells(i, 28))
            End With
            End If
            i = i + 1
        Loop
            
       End If
       
    If obj_addItemForm.checkBox_noDwg = True Then
        With obj_addItemForm
            logs_data = Array(.textBox_Code, .textBox_Name, "������� ���������� ��")
        End With
    Else
        With form
            logs_data = Array(.textBox_Code, .textBox_Name, "������� ����������")
        End With
    End If
    drop_logs logs_data
    
End Sub

Public Sub tips_update(ByRef form As UserForm)
    '���������� ���������� � ����������� ��������, �������� ����, ���������, ��������� ����
Dim s As Long

    With ActiveWorkbook.Worksheets("arr_Parts")
        s = .Cells(Rows.count, 1).End(xlUp).row
        If WorksheetFunction.CountA(.Range(.Cells(4, 12), .Cells(s + 1, 12))) > 0 Then form.cb_userbar.List = .Range(.Cells(4, 12), .Cells(s + 1, 12)).value
        If WorksheetFunction.CountA(.Range(.Cells(4, 13), .Cells(s + 1, 13))) > 0 Then form.cb_userbarGOST.List = .Range(.Cells(4, 13), .Cells(s + 1, 13)).value
        If WorksheetFunction.CountA(.Range(.Cells(4, 10), .Cells(s + 1, 10))) > 0 Then form.cb_usermaterial.List = .Range(.Cells(4, 10), .Cells(s + 1, 10)).value
        If WorksheetFunction.CountA(.Range(.Cells(4, 11), .Cells(s + 1, 11))) > 0 Then form.cb_usermaterialGOST.List = .Range(.Cells(4, 11), .Cells(s + 1, 11)).value
    End With
End Sub

Public Sub tips_update_paint(ByRef form As UserForm)
    '���������� ���������� � ����������� ��������, ��������_����
Dim s As Long
Dim paint_collection As New Collection
Dim myCell, myRange As Range
Dim myElement As Variant

    With ThisWorkbook.Worksheets("db_paint")
        s = .Cells(Rows.count, 1).End(xlUp).row
        Set myRange = .Range(.Cells(4, 2), .Cells(s + 1, 2))
        
            On Error Resume Next
                For Each myCell In myRange
                    paint_collection.Add CStr(myCell.value), CStr(myCell.value)
                Next myCell
            On Error GoTo 0
        
        form.cb_paint.Clear
        For Each myElement In paint_collection
            form.cb_paint.AddItem myElement
        Next myElement
        'If WorksheetFunction.CountA(.Range(.Cells(4, 8), .Cells(s + 1, 8))) > 0 Then form.cb_paintGOST.List = .Range(.Cells(4, 8), .Cells(s + 1, 8)).value
    End With
End Sub

Public Sub tips_update_additem(ByRef form As UserForm, ByRef sheet As Worksheet)
    '���������� ���������� � ����������� �����������, ������������, ��������� � ����
Dim s As Long

    With sheet
        s = .Cells(Rows.count, 1).End(xlUp).row
        If WorksheetFunction.CountA(.Range(.Cells(4, 2), .Cells(s + 1, 2))) > 0 Then form.textBox_Code.List = .Range(.Cells(4, 2), .Cells(s + 1, 2)).value
        If WorksheetFunction.CountA(.Range(.Cells(4, 3), .Cells(s + 1, 3))) > 0 Then form.textBox_Name.List = .Range(.Cells(4, 3), .Cells(s + 1, 3)).value
        If WorksheetFunction.CountA(.Range(.Cells(4, 8), .Cells(s + 1, 8))) > 0 Then form.textBox_GOST.List = .Range(.Cells(4, 8), .Cells(s + 1, 8)).value
        If WorksheetFunction.CountA(.Range(.Cells(4, 4), .Cells(s + 1, 4))) > 0 Then form.textBox_In.List = .Range(.Cells(4, 4), .Cells(s + 1, 4)).value
    End With
    
End Sub

Public Sub cb_density_display(ByRef form As UserForm)
    Dim i As Long
    form.cb_density.Clear
    For i = 0 To UBound(db_density) - 1
        form.cb_density.AddItem CStr(db_density(i).density) + vbTab + db_density(i).material
    Next i
    form.cb_density.value = form.cb_density.List(8)
End Sub

Public Sub cb_bar_display(ByRef form As UserForm)
    form.cb_bar.List = Array( _
                            "���� / ������      ", _
                            "����               ", _
                            "������������       ", _
                            "������             ", _
                            "����� �������      ", _
                            "����� �������������", _
                            "������� ������" _
                                                            )  '������� ���������
End Sub

Public Sub clean_propInput_form()
    With mainModule.obj_propInputForm
        .textBox_Name.Text = ""
        .textBox_Code.Text = ""
        .textBox_SumAmount = ""
        
        .cb_userbar.Text = ""
        .cb_userbarGOST.Text = ""
        .cb_usermaterial.Text = ""
        .cb_usermaterialGOST.Text = ""
        
        .cb_bar.ListIndex = -1
        .cb_density.ListIndex = -1
        
        .textBox_A.Text = ""
        .textBox_B.Text = ""
        .textBox_C.Text = ""
        .textBox_D.Text = ""
        
        .textBox_ExactWeight.Text = ""
    End With
End Sub

Public Sub select_0(ByRef form As UserForm)
    '����� �����/������
    With form
        .textBox_A.Visible = True
        .textBox_B.Visible = True
        .textBox_C.Visible = False
        .textBox_D.Visible = False
        
        .textBox_A.Text = "D"
        .textBox_B.Text = "L"
        
        .Label13.Visible = False
        .Label19.Visible = False
    End With
End Sub

Public Sub select_1(ByRef form As UserForm)
    '����� �����
    With form
        .textBox_A.Visible = True
        .textBox_B.Visible = True
        .textBox_C.Visible = True
        .textBox_D.Visible = False
        
        .textBox_A.Text = "�"
        .textBox_B.Text = "�"
        .textBox_C.Text = "�"
        
        .Label13.Visible = True
        .Label19.Visible = False
    End With
End Sub

Public Sub select_2(ByRef form As UserForm)
    '����� �������������
    With form
        .textBox_A.Visible = True
        .textBox_B.Visible = True
        .textBox_C.Visible = False
        .textBox_D.Visible = False
        
        .textBox_A.Text = "H"
        .textBox_B.Text = "L"
        
        .Label13.Visible = False
        .Label19.Visible = False
    End With
End Sub

Public Sub select_3(ByRef form As UserForm)
    '����� ������
    With form
        .textBox_A.Visible = True
        .textBox_B.Visible = True
        .textBox_C.Visible = True
        .textBox_D.Visible = True
        
        .textBox_A.Text = "A"
        .textBox_B.Text = "B"
        .textBox_C.Text = "S"
        .textBox_D.Text = "L"
        
        .Label13.Visible = True
        .Label19.Visible = True
    End With
End Sub

Public Sub select_4(ByRef form As UserForm)
    '����� ������� �����
    With form
        .textBox_A.Visible = True
        .textBox_B.Visible = True
        .textBox_C.Visible = True
        .textBox_D.Visible = False
        
        .textBox_A.Text = "D"
        .textBox_B.Text = "S"
        .textBox_C.Text = "L"
        
        .Label13.Visible = True
        .Label19.Visible = False
    End With
End Sub

Public Sub select_5(ByRef form As UserForm)
    '����� ������������� �����
    With form
        .textBox_A.Visible = True
        .textBox_B.Visible = True
        .textBox_C.Visible = True
        .textBox_D.Visible = True
        
        .textBox_A.Text = "A"
        .textBox_B.Text = "B"
        .textBox_C.Text = "S"
        .textBox_D.Text = "L"
        
        .Label13.Visible = True
        .Label19.Visible = True
    End With
End Sub

Public Sub select_6(ByRef form As UserForm)
    '����� �������� �������
    With form
        .textBox_A.Visible = True
        .textBox_B.Visible = True
        .textBox_C.Visible = False
        .textBox_D.Visible = False
        
        .textBox_A.Text = "m"
        .textBox_B.Text = "L"
        
        .Label13.Visible = False
        .Label19.Visible = False
    End With
End Sub
