Attribute VB_Name = "processModule"
Option Explicit

Public Sub count_the_sums(ByRef sheet As Worksheet)    '��������� �������� ���� ��������� � ���������� ����� overall_amount � sum_amount
    Dim i As Long
    
    For i = 4 To num_of_strings(sheet) + 4
        sheet.Cells(i, 6) = count_overall_amount(i, sheet)
    Next i
    
    For i = 4 To num_of_strings(sheet) + 4
        sheet.Cells(i, 7) = count_sum_amount(i, sheet)
    Next i
End Sub

Public Sub count_the_sums_materials(ByRef sheet As Worksheet)    '��������� �������� ���� ��������� � ���������� ����� overall_amount � sum_amount ��� ����������,
    ' ������� ����� ������� �� ������ � ������, �� � � ������
    
    Dim i As Long
    
    For i = 4 To num_of_strings(sheet) + 4
        sheet.Cells(i, 6) = count_overall_amount_materials(i, sheet)
    Next i
    
    For i = 4 To num_of_strings(sheet) + 4
        If CLng(sheet.Cells(i, 24)) <> 0 Then
            sheet.Cells(i, 7) = sheet.Cells(i, 6)
        Else
        sheet.Cells(i, 7) = count_sum_amount(i, sheet)
        End If
    Next i
End Sub

Public Sub bubble_sort(ByRef sheet As Worksheet, ByVal column_num As Long)
    '���������� �� column_num-���� �������
    Dim i, j, last As Long
        
    last = num_of_strings(sheet) + 4
        
    For i = 4 To last - 1
        For j = i + 1 To last - 1
            If CStr(sheet.Cells(i, column_num) & sheet.Cells(i, 8)) = CStr(sheet.Cells(j, column_num) & sheet.Cells(j, 8)) Then
                If CStr(sheet.Cells(i, column_num) & sheet.Cells(i, 4)) > CStr(sheet.Cells(j, column_num) & sheet.Cells(j, 4)) Then
                    strings_swap sheet, j, i
                End If
            Else
                If CStr(sheet.Cells(i, column_num) & sheet.Cells(i, 8)) > CStr(sheet.Cells(j, column_num) & sheet.Cells(j, 8)) Then
                    strings_swap sheet, j, i
                End If
            End If
        Next j
    Next i
End Sub

Public Sub bubble_sort_for_materials(ByRef sheet As Worksheet, ByVal column_num As Long)
    '���������� �� column_num-���� �������
    Dim i, j, last As Long
        
    last = num_of_strings(sheet) + 4
        
    For i = 4 To last - 1
        For j = i + 1 To last - 1
            If sheet.Cells(i, 2) & sheet.Cells(i, 4) > sheet.Cells(j, 2) & sheet.Cells(j, 4) Then
                strings_swap sheet, j, i
            End If
        Next j
    Next i
End Sub

Public Sub strings_swap(ByRef sheet As Worksheet, ByVal M As Long, ByVal n As Long)
    Dim temp(30) As Variant
    Dim i As Long
    
    For i = 0 To 30
        If sheet.Cells(M, i + 1) <> "" Or sheet.Cells(n, i + 1) <> "" Then
            temp(i) = sheet.Cells(M, i + 1)
            sheet.Cells(M, i + 1) = sheet.Cells(n, i + 1)
            sheet.Cells(n, i + 1) = temp(i)
        End If
    Next i
End Sub

Public Function count_overall_amount(ByVal index As Long, sheet As Worksheet) As Double
'������� ������ ���������� �� ����� ��� �������� ����� sheet ��� ������� index

    Dim i, summer As Long
    'summer - ��������
   
    If sheet.Cells(index, 4) = "" Then
        count_overall_amount = sheet.Cells(index, 5)
    '���� ������� ������ �� ������, �� ��� �������� ������, � �� ���������� �� ����� ����� ���������� �� ������������
    Else
        summer = 0
        i = 4
        Do While ActiveWorkbook.Worksheets("arr_SubAssemblies").Cells(i, 1) <> 0
            
            If ActiveWorkbook.Worksheets("arr_SubAssemblies").Cells(i, 2) = sheet.Cells(index, 4) Then
                summer = summer + count_overall_amount(i, ActiveWorkbook.Worksheets("arr_SubAssemblies"))
            End If
            
            i = i + 1
        Loop
        
        i = 4
        Do While ActiveWorkbook.Worksheets("arr_RootAssemblies").Cells(i, 1) <> 0
            
            If ActiveWorkbook.Worksheets("arr_RootAssemblies").Cells(i, 2) = sheet.Cells(index, 4) Then
                summer = summer + ActiveWorkbook.Worksheets("arr_RootAssemblies").Cells(i, 5)
            End If
            
            i = i + 1
        Loop
        
    count_overall_amount = summer * CDbl(sheet.Cells(index, 5))
    End If
End Function

Public Function count_overall_amount_materials(ByVal index As Long, sheet As Worksheet) As Double
'������� ������ ���������� �� ����� ��� �������� ����� sheet ��� ������� index

    Dim i, summer As Long
    'summer - ��������
   
        summer = 0
        i = 4
        
        Do While ActiveWorkbook.Worksheets("arr_SubAssemblies").Cells(i, 1) <> 0
            
            If ActiveWorkbook.Worksheets("arr_SubAssemblies").Cells(i, 2) = sheet.Cells(index, 4) Then
                summer = summer + count_overall_amount_materials(i, ActiveWorkbook.Worksheets("arr_SubAssemblies"))
            End If
            
            i = i + 1
        Loop
        
        i = 4
        Do While ActiveWorkbook.Worksheets("arr_RootAssemblies").Cells(i, 1) <> 0
            
            If ActiveWorkbook.Worksheets("arr_RootAssemblies").Cells(i, 2) = sheet.Cells(index, 4) Then
                summer = summer + ActiveWorkbook.Worksheets("arr_RootAssemblies").Cells(i, 5)
            End If
            
            i = i + 1
        Loop
        
        i = 4
        Do While ActiveWorkbook.Worksheets("arr_Parts").Cells(i, 1) <> 0
            
            If ActiveWorkbook.Worksheets("arr_Parts").Cells(i, 2) = sheet.Cells(index, 4) Then
                summer = summer + count_overall_amount_materials(i, ActiveWorkbook.Worksheets("arr_Parts"))
            End If
            
            i = i + 1
        Loop
        
    count_overall_amount_materials = summer * CDbl(sheet.Cells(index, 5))
End Function


Public Function count_sum_amount(ByRef index As Long, ByRef sheet As Worksheet) As Double
    '�������, ������� ����� ������� ����� sheet � �������� index � ��������� ���� overall_amount ��� ���� ���������� �����������
    
    Dim i As Long
    Dim sum As Double
    
    sum = 0
    i = 4
    
    Do While sheet.Cells(i, 1) <> ""
        If CStr(sheet.Cells(i, 2)) = CStr(sheet.Cells(index, 2)) And _
            CStr(sheet.Cells(i, 8)) = CStr(sheet.Cells(index, 8)) Then sum = sum + CDbl(sheet.Cells(i, 6))
        i = i + 1
    Loop
    
    count_sum_amount = sum
End Function

Public Sub integrate_materials()
    '��������� ���������� ���������� � ���������� ���������� � ������� � ������� �� �� ���� integrated_materials
On Error GoTo instr1
    
Dim p_parts As Long   '���������� ��� �������� �� ����� arr_Parts
Dim p_materials As Long '���������� ��� �������� �� ����� � ������ materials_integrated (p - "pointer")

Dim index As Long   '����� ���������� �������� �� ����� materials_integrated
Dim exist As Boolean    '���� �� ����������

Dim norma As Double '�-� �����

p_parts = 4
With ActiveWorkbook
.Worksheets("integrated_materials").Range("A4:I999").Clear
.Worksheets("integrated_materials").Range("B1:E999").NumberFormat = "@"
    
    Do While .Worksheets("arr_Parts").Cells(p_parts, 1) <> ""
      If p_parts = 4 Or .Worksheets("arr_Parts").Cells(p_parts, 2) <> .Worksheets("arr_Parts").Cells(p_parts - 1, 2) Then
        '��������� ���� �� ��� � ������ materials_integrated ������� � ������ ������
        '���� ���� - ���������� ��� ������ � ������������ ������������ �������� � �������� ����� �� ���������� ������� ���������
        '���� ��� - �������� � �����
        
        
        find_by_material .Worksheets("arr_Parts").Cells(p_parts, 10), _
                            .Worksheets("arr_Parts").Cells(p_parts, 11), _
                            .Worksheets("arr_Parts").Cells(p_parts, 12), _
                            .Worksheets("arr_Parts").Cells(p_parts, 13), _
                            index, exist

            Select Case .Worksheets("arr_Parts").Cells(p_parts, 14)
                Case 0, 2, 3, 4, 5, 6: norma = 1.1
                Case 1: norma = 1.15
            End Select
            
        If exist And .Worksheets("arr_Parts").Cells(p_parts, 24) = 0 Then
        
            .Worksheets("integrated_materials").Cells(index, 6) = _
                   Round(CDbl(.Worksheets("integrated_materials").Cells(index, 6)) + CDbl(.Worksheets("arr_Parts").Cells(p_parts, 9)) * CDbl(.Worksheets("arr_Parts").Cells(p_parts, 7)), 2)
            .Worksheets("integrated_materials").Cells(index, 8) = _
                   Round(CDbl(.Worksheets("integrated_materials").Cells(index, 8)) + CDbl(.Worksheets("arr_Parts").Cells(p_parts, 22)) * CDbl(.Worksheets("arr_Parts").Cells(p_parts, 7)), 2)
                        
            .Worksheets("integrated_materials").Cells(index, 7) = Round(norma * CDbl(.Worksheets("integrated_materials").Cells(index, 6)), 2)
            .Worksheets("integrated_materials").Cells(index, 9) = Round(norma * CDbl(.Worksheets("integrated_materials").Cells(index, 8)), 2)

        ElseIf .Worksheets("arr_Parts").Cells(p_parts, 24) = 0 Then
            .Worksheets("integrated_materials").Cells(index, 1) = Date & " " & Time()
            .Worksheets("integrated_materials").Cells(index, 2) = CStr(.Worksheets("arr_Parts").Cells(p_parts, 12))
            .Worksheets("integrated_materials").Cells(index, 3) = CStr(.Worksheets("arr_Parts").Cells(p_parts, 13))
            .Worksheets("integrated_materials").Cells(index, 4) = CStr(.Worksheets("arr_Parts").Cells(p_parts, 10))
            .Worksheets("integrated_materials").Cells(index, 5) = CStr(.Worksheets("arr_Parts").Cells(p_parts, 11))
            
            If Round(CDbl(.Worksheets("arr_Parts").Cells(p_parts, 7)) * CDbl(.Worksheets("arr_Parts").Cells(p_parts, 9)), 2) < 0.001 Then
                .Worksheets("integrated_materials").Cells(index, 6) = 0.001
            Else: .Worksheets("integrated_materials").Cells(index, 6) = Round(CDbl(.Worksheets("arr_Parts").Cells(p_parts, 7)) * CDbl(.Worksheets("arr_Parts").Cells(p_parts, 9)), 2)
            End If
            If Round(CDbl(.Worksheets("arr_Parts").Cells(p_parts, 7)) * CDbl(.Worksheets("arr_Parts").Cells(p_parts, 22)), 2) < 0.001 Then
                .Worksheets("integrated_materials").Cells(index, 8) = 0.001
            Else: .Worksheets("integrated_materials").Cells(index, 8) = Round(CDbl(.Worksheets("arr_Parts").Cells(p_parts, 7)) * CDbl(.Worksheets("arr_Parts").Cells(p_parts, 22)), 2)
            End If
            If Round(norma * CDbl(.Worksheets("integrated_materials").Cells(index, 6)), 2) < 0.001 Then
            .Worksheets("integrated_materials").Cells(index, 7) = 0.001
            Else: .Worksheets("integrated_materials").Cells(index, 7) = Round(norma * CDbl(.Worksheets("integrated_materials").Cells(index, 6)), 2)
            End If
            If Round(norma * CDbl(.Worksheets("integrated_materials").Cells(index, 8)), 2) < 0.001 Then
            .Worksheets("integrated_materials").Cells(index, 9) = 0.001
            Else: .Worksheets("integrated_materials").Cells(index, 9) = Round(norma * CDbl(.Worksheets("integrated_materials").Cells(index, 8)), 2)
            End If
        End If
        End If
        p_parts = p_parts + 1
    Loop
   bubble_sort_for_materials .Worksheets("integrated_materials"), 2
   ActiveWorkbook.Worksheets("integrated_materials").Visible = True
End With

logs_data = Array("������������ �������")
drop_logs logs_data
Exit Sub
        
instr1:
    MsgBox "������! ���������� ������ �� ������� �������"
End Sub

Public Sub find_by_material(ByVal mat, matgost, bar, bargost As String, ByRef index As Long, ByRef exist As Boolean)
    index = 4
    exist = False
    With ActiveWorkbook.Worksheets("integrated_materials")
        Do While .Cells(index, 1) <> ""
            If .Cells(index, 2) = bar And .Cells(index, 3) = bargost And _
                .Cells(index, 4) = mat And .Cells(index, 5) = matgost Then
                exist = True
                Exit Do
            Else
                index = index + 1
            End If
        Loop
    End With
End Sub


