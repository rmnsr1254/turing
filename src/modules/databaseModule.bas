Attribute VB_Name = "databaseModule"
Option Explicit

Type density_rec
    density As Double
    material As String
End Type

Public db_density() As density_rec
Public db_bar() As String

Public Sub fill_db_density(ByRef db() As density_rec)
    Dim i As Long
            
    ReDim db(0)
    i = 3
    
    Do While ActiveWorkbook.Worksheets("db_density").Cells(i, 1) <> ""
        ReDim Preserve db(UBound(db_density) + 1)
        db(i - 3).material = ActiveWorkbook.Worksheets("db_density").Cells(i, 1)
        db(i - 3).density = CDbl(ActiveWorkbook.Worksheets("db_density").Cells(i, 2))
        i = i + 1
    Loop
End Sub






