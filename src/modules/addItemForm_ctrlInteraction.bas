Attribute VB_Name = "addItemForm_ctrlInteraction"
'� ���� ������ ��������� �������������� ������ ����� ���� � ������

Option Explicit

Public Sub listbox_update_added(ByVal sheet_name As String, ByVal code, name As String)
    '���������� ��������� ����� ��������������
    With mainModule.obj_addItemForm.ListBox1
        Select Case sheet_name
            Case "arr_RootAssemblies": .AddItem (Date & " " & Time() & " �������� ������ '" & code & " - " & name & "' ��������� �������!")
            Case "arr_SubAssemblies": .AddItem (Date & " " & Time() & " ��������� '" & code & " - " & name & "' ��������� �������!")
            Case "arr_Parts": .AddItem (Date & " " & Time() & " ������ '" & code & " - " & name & "' ��������� �������!")
            Case "arr_StandartParts": .AddItem (Date & " " & Time() & " ����������� ������ '" & code & " - " & name & "' ��������� �������!")
            Case "arr_Materials": .AddItem (Date & " " & Time() & " �������� '" & code & " - " & name & "' �������� �������!")
            Case "arr_Others": .AddItem (Date & " " & Time() & " ������ ������� '" & code & " - " & name & "' ��������� �������!")
        End Select
            .ListIndex = .ListCount - 1
    End With
End Sub

Public Sub clean_the_form()     '�������� ����� ����������
    With obj_addItemForm
        '.textBox_Code = ""
        '.textBox_Amount = ""
        '.textBox_In = ""
        '.textBox_Name = ""
       ' .textBox_GOST = ""
    End With
End Sub

Public Sub select_Material()
With mainModule.obj_addItemForm
    .ob_RootAssembly.value = False
    .ob_SubAssembly.value = False
    .ob_Part.value = False
    .ob_StandartPart = False
    .ob_Material = True
    .ob_Others = False
    
    .textBox_Name.Enabled = False
    .textBox_Code.Enabled = True
    .textBox_Amount.Enabled = True
    .textBox_In.Enabled = True
    .textBox_GOST.Enabled = True
    .checkBox_noDwg.Enabled = False
    .checkBox_noDwg.value = False
    .checkBox_Painted = False
    .checkBox_Painted.Enabled = False
    
    .cb_Units.Enabled = True
    .cb_Units.List = Array("��", "��", "�", "�2", "�3")
    .cb_Units.ListIndex = 0

    .textBox_Name.value = ""

    End With
    tips_update_additem mainModule.obj_addItemForm, ActiveWorkbook.Worksheets("arr_Materials")

    logs_data = Array("������� ���������")
    drop_logs logs_data

End Sub

Public Sub select_Part()
With mainModule.obj_addItemForm
    .ob_RootAssembly.value = False
    .ob_SubAssembly.value = False
    .ob_Part.value = True
    .ob_StandartPart = False
    .ob_Material = False
    .ob_Others = False
    
    .textBox_Name.Enabled = True
    .textBox_Code.Enabled = True
    .textBox_Amount.Enabled = True
    .textBox_In.Enabled = True
    .textBox_GOST.Enabled = True
    .checkBox_noDwg.Enabled = True
    .checkBox_Painted = False
    .checkBox_Painted.Enabled = False
    
    .cb_Units.Enabled = False
    .cb_Units.Clear

    End With
    tips_update_additem mainModule.obj_addItemForm, ActiveWorkbook.Worksheets("arr_Parts")

    logs_data = Array("������� ������")
    drop_logs logs_data

End Sub

Public Sub select_RootAssembly()
With mainModule.obj_addItemForm
    .ob_RootAssembly.value = True
    .ob_SubAssembly.value = False
    .ob_Part.value = False
    .ob_StandartPart = False
    .ob_Material = False
    .ob_Others = False
    .textBox_GOST.Enabled = True
    .textBox_In.value = ""
    
    .textBox_In.Enabled = False
    .textBox_Name.Enabled = True
    .textBox_Code.Enabled = True
    .textBox_Amount.Enabled = True
    .checkBox_noDwg.Enabled = False
    .checkBox_noDwg.value = False
    .checkBox_Painted.Enabled = True
    
    
    .cb_Units.Enabled = False
    .cb_Units.Clear
    
End With
    tips_update_additem mainModule.obj_addItemForm, ActiveWorkbook.Worksheets("arr_RootAssemblies")

    logs_data = Array("������� �������� ������")
    drop_logs logs_data

End Sub

Public Sub select_StandartPart()
With mainModule.obj_addItemForm
    .ob_RootAssembly.value = False
    .ob_SubAssembly.value = False
    .ob_Part.value = False
    .ob_StandartPart = True
    .ob_Material = False
    .ob_Others = False
    
    .textBox_Name.Enabled = False
    .textBox_Code.Enabled = True
    .textBox_Amount.Enabled = True
    .textBox_In.Enabled = True
    .textBox_GOST.Enabled = True
    .checkBox_noDwg.Enabled = False
    .checkBox_noDwg.value = False
    .checkBox_Painted = False
    .checkBox_Painted.Enabled = False
    .textBox_Name.value = ""
    
    
    .cb_Units.Enabled = False
    .cb_Units.Clear

    End With
    tips_update_additem mainModule.obj_addItemForm, ActiveWorkbook.Worksheets("arr_StandartParts")

    logs_data = Array("������� ����������� ������")
    drop_logs logs_data

End Sub

Public Sub select_SubAssembly()
With mainModule.obj_addItemForm
    .ob_RootAssembly.value = False
    .ob_SubAssembly.value = True
    .ob_Part.value = False
    .ob_StandartPart = False
    .ob_Material = False
    .ob_Others = False
    
    .textBox_Name.Enabled = True
    .textBox_Code.Enabled = True
    .textBox_Amount.Enabled = True
    .textBox_In.Enabled = True
    .textBox_GOST.Enabled = True
    .checkBox_noDwg.Enabled = False
    .checkBox_noDwg.value = False
    .checkBox_Painted.Enabled = True
    
    
    .cb_Units.Enabled = False
    .cb_Units.Clear

    End With
    tips_update_additem mainModule.obj_addItemForm, ActiveWorkbook.Worksheets("arr_SubAssemblies")

    logs_data = Array("������� ���������")
    drop_logs logs_data

End Sub

Public Sub select_Others()
With mainModule.obj_addItemForm
    .ob_RootAssembly.value = False
    .ob_SubAssembly.value = False
    .ob_Part.value = False
    .ob_StandartPart = False
    .ob_Material = False
    .ob_Others = True
    
    .textBox_Name.Enabled = False
    .textBox_Code.Enabled = True
    .textBox_Amount.Enabled = True
    .textBox_In.Enabled = True
    .textBox_GOST.Enabled = True
    .checkBox_noDwg.Enabled = False
    .checkBox_noDwg.value = False
    .checkBox_Painted.Enabled = False
    .textBox_Name.value = ""
    
    
    .cb_Units.Enabled = False
    .cb_Units.Clear

    End With
    tips_update_additem mainModule.obj_addItemForm, ActiveWorkbook.Worksheets("arr_Others")

    logs_data = Array("������� ������ �������")
    drop_logs logs_data

End Sub

Public Sub select_RootAssemblies_toEdit()
With mainModule.obj_editDeleteForm
    .textBox_Name.Enabled = True
    .textBox_Code.Enabled = True
    .textBox_In.Enabled = False
    .textBox_Amount.Enabled = True
    .textBox_GOST.Enabled = False
End With
End Sub

Public Sub select_SubAssemblies_toEdit()
With mainModule.obj_editDeleteForm
    .textBox_Name.Enabled = True
    .textBox_Code.Enabled = True
    .textBox_In.Enabled = True
    .textBox_Amount.Enabled = True
    .textBox_GOST.Enabled = False
End With
End Sub

Public Sub select_Parts_toEdit()
With mainModule.obj_editDeleteForm
    .textBox_Name.Enabled = True
    .textBox_Code.Enabled = True
    .textBox_In.Enabled = True
    .textBox_Amount.Enabled = True
    .textBox_GOST.Enabled = False
End With
End Sub

Public Sub select_StandartParts_toEdit()
With mainModule.obj_editDeleteForm
    .textBox_Name.Enabled = False
    .textBox_Code.Enabled = True
    .textBox_In.Enabled = True
    .textBox_Amount.Enabled = True
    .textBox_GOST.Enabled = True
End With
End Sub

Public Sub select_Materials_toEdit()
With mainModule.obj_editDeleteForm
    .textBox_Name.Enabled = False
    .textBox_Code.Enabled = True
    .textBox_In.Enabled = True
    .textBox_Amount.Enabled = True
    .textBox_GOST.Enabled = True
End With
End Sub

Public Sub clean_edit_form()
With mainModule.obj_editDeleteForm
    .textBox_Name.value = ""
    .textBox_Code.value = ""
    .textBox_In.value = ""
    .textBox_Amount.value = ""
    .textBox_GOST.value = ""
    
    .textBox_Name.Enabled = False
    .textBox_Code.Enabled = False
    .textBox_In.Enabled = False
    .textBox_Amount.Enabled = False
    .textBox_GOST.Enabled = False
End With
End Sub
