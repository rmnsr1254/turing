Attribute VB_Name = "addItemForm_errorsModule"
Option Explicit

Public Function if_RootAssembly_Filled(ByVal code As String, _
            ByVal name As String, ByVal Amount As String) As Boolean     '�������� ������������� ����� ��� �������� ������

    If code <> "" And name <> "" And Amount <> "" Then if_RootAssembly_Filled = True
    
End Function

Public Function if_SubAssemblyOrPart_Filled(ByVal code As String, ByVal name As String, _
             ByVal into As String, ByVal Amount As String) As Boolean     '�������� ������������� ����� ��� ��������� ��� ������

    If code <> "" And into <> "" And name <> "" And Amount <> "" Then if_SubAssemblyOrPart_Filled = True
    
End Function

Public Function if_StdPartOrMatOrOth_Filled(ByVal code As String, ByVal into As String, _
            ByVal Amount As String, ByVal gost As String) As Boolean     '�������� ������������� ����� ��� ����������� ������, �������� ��� ������ �������

    If code <> "" And gost <> "" And Amount <> "" Then if_StdPartOrMatOrOth_Filled = True
End Function

Public Function if_In_Assembly_exists(ByRef In_code_ToCheck As String, _
                                        ByRef SheetToLookIn As Worksheet) As Boolean
                                     'In_code_ToCheck - ����������� �������� ��� �������� ���� ��������� �������������
                                     '������ �������� ������
                                     'SheetToLookIn - ����, � ������� ���� ������ ������ �������� ������
    Dim i As Long
    if_In_Assembly_exists = False
    
    i = 3
    
    Do While SheetToLookIn.Cells(i, 1) <> ""
        If In_code_ToCheck = SheetToLookIn.Cells(i, 2) Then
            if_In_Assembly_exists = True
            Exit Function
        End If
        i = i + 1
    Loop
    
End Function

Public Function if_doubled(ByVal code As String, ByVal is_in As String, ByVal gost As String, _
                            ByRef SheetToLookIn As Worksheet) As Boolean
                            '���������, �� ����������� �� ������� � ������������ code
                            '� ����������� is_in � ����� sheettolookin
    Dim i As Long
    
    i = 3
    if_doubled = False
    
    Do While SheetToLookIn.Cells(i, 1) <> ""
        If RTrim(code) = SheetToLookIn.Cells(i, 2) And is_in = SheetToLookIn.Cells(i, 4) And gost = SheetToLookIn.Cells(i, 8) Then
            if_doubled = True
            Exit Function
        End If
        i = i + 1
    Loop
    
End Function

Public Function if_paint_Filled(ByVal textBox_paint, textBox_paintGOST, textBox_Cons, textBox_ConsCoeff, textBox_Area, layers_num As String)
    With mainModule.obj_propInputForm
    If textBox_paint <> "" And (textBox_paintGOST <> "" Or .cb_paintGOST.Enabled = False) _
        And (textBox_Cons <> "" Or .textBox_Cons.Enabled = False) _
        And (textBox_ConsCoeff <> "" Or .textBox_ConsCoeff.Enabled = False) _
        And textBox_Area <> "" _
        And (layers_num <> "" Or .textBox_layersNum.Enabled = False) Then
            if_paint_Filled = True
    Else: if_paint_Filled = False
    End If
    End With
End Function




                                    
                                    

